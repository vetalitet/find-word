package com.vetalitet.findword.utils

import android.content.Context
import android.graphics.Typeface

class FontUtils {

    companion object {
        fun loadCustomFont(context: Context, asset: String) : Typeface {
            return Typeface.createFromAsset(context.assets, asset)
        }
    }

}
