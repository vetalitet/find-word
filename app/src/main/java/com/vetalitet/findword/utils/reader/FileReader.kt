package com.vetalitet.findword.utils.reader

import android.content.Context
import android.util.Base64
import com.vetalitet.findword.data.components.RectStatus
import com.vetalitet.findword.data.objects.*
import java.io.*
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class FileReader(private val context: Context) {

    companion object {
        val MENUS = "menus"
        val CHART_SIZE_REGEX = """(\d)x(\d)""".toRegex()
        val ITEM_REGEX = """(\d)/(\d)/([a-zA-Z])""".toRegex()
    }

    fun readData(lastExistingMenu: Int): List<MenuItemData> {
        val resultMenuItems: ArrayList<MenuItemData> = ArrayList()

        val menuItemsFromFile = getAllMenuItems()
        menuItemsFromFile?.let {
            menuItemsFromFile
                    .map { Pair(it.split(".")[0].toInt(), it.split(".")[1]) }
                    .filter { it.first > lastExistingMenu }
                    .forEach {
                        val levels = parseFile("$MENUS/${it.first}.${it.second}", it.first)
                        resultMenuItems.add(MenuItemData(it.first, it.second, levels))
                    }
        }

        return resultMenuItems
    }

    private fun parseFile(fileName: String, menuItemNumber: Int): ArrayList<LevelData> {
        var width = 0
        var height = 0
        var levelId = 1
        var readNextNLines = 0

        val levels: ArrayList<LevelData> = ArrayList()
        var words: ArrayList<WordData> = ArrayList()
        var items: ArrayList<ItemData> = ArrayList()

        val inputStream = context.assets.open(fileName)
        val decrypt = decrypt(inputStream)

        val lines = decrypt.split("\r\n")

        var charNumber = 0
        var wordNumber = 1

        for (i in 0 until lines.size) {
            val line = lines[i]

            if (line.startsWith("//")) continue

            // check symbols consequence
            val result = CHART_SIZE_REGEX.find(line)
            if (result != null) {
                width = result.groups[1]?.value?.toInt()!!
                height = result.groups[2]?.value?.toInt()!!
                readNextNLines = width * height
            } else {
                if (line != "") {
                    items.add(prepareItem(ITEM_REGEX.find(line)!!, charNumber, wordNumber))
                    charNumber++
                    readNextNLines--

                    if (readNextNLines == 0) {
                        if (!items.isEmpty()) {
                            words.add(WordData(items))
                            items = ArrayList()
                            wordNumber++
                        }
                    }
                } else {
                    if (!items.isEmpty()) {
                        words.add(WordData(items))
                        items = ArrayList()
                        wordNumber++
                    }
                }
            }

            if (!words.isEmpty() && readNextNLines == 0) {
                levels.add(LevelData(ChartSizeData(width, height), levelId, menuItemNumber, words))
                words = ArrayList()
                levelId++
                wordNumber = 1
            }
        }
        return levels
    }

    private val key = byteArrayOf(0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1)
    private val iv = byteArrayOf(1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0)

    var encryptCipher: Cipher? = null
    var decryptCipher: Cipher? = null

    fun decrypt(stream: InputStream): String {
        encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        encryptCipher?.init(Cipher.ENCRYPT_MODE, SecretKeySpec( key, "AES"),IvParameterSpec(iv))
        decryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        decryptCipher?.init(Cipher.DECRYPT_MODE, SecretKeySpec( key, "AES"), IvParameterSpec(iv))

        val decryptedPayloadOutputStream = ByteArrayOutputStream()
        CipherInputStream(stream, decryptCipher).use({ inputStream -> decryptedPayloadOutputStream.use({ outputStream -> copy(inputStream, outputStream) }) })
        val decryptedPayload = decryptedPayloadOutputStream.toByteArray()
        return String(decryptedPayload)
    }

    @Throws(IOException::class)
    fun copy(input: InputStream, output: OutputStream) {

        //Creating byte array
        val buffer = ByteArray(1024)
        var length = input.read(buffer)

        //Transferring data
        while(length != -1) {
            output.write(buffer, 0, length)
            length = input.read(buffer)
        }

        //Finalizing
        output.flush()
        output.close()
        input.close()
    }

    private fun prepareItem(result: MatchResult, charNumber: Int, wordNumber: Int): ItemData {
        val x = result.groups[1]?.value?.toInt()!!
        val y = result.groups[2]?.value?.toInt()!!
        val char = result.groups[3]?.value?.single()!!
        return ItemData(x, y, charNumber, wordNumber, RectStatus.None.value, 0, char)
    }

    private fun getAllMenuItems(): Array<out String>? {
        return context.assets.list(MENUS)
    }

}
