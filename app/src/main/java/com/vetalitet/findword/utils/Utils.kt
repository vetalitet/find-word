package com.vetalitet.findword.utils

import java.text.SimpleDateFormat
import java.util.*

object Utils {

    private val random = Random()

    fun rand(from: Int, to: Int): Int {
        return random.nextInt(to - from) + from
    }

    fun toTimeFormat(date: Date): String {
        val sdf = SimpleDateFormat("HH:mm yyyy-MM-dd", Locale.getDefault())
        return sdf.format(date)
    }

}
