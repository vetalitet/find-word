package com.vetalitet.findword.utils.helpers

import android.content.Context
import android.content.SharedPreferences

class SharedPreferencesManager(context: Context) {

    val PREFS_FILENAME = "com.teamtreehouse.colorsarefun.prefs"

    val prefs: SharedPreferences?

    init {
        prefs = context.getSharedPreferences(PREFS_FILENAME, 0)
    }

    fun getCurrentColorIndex(menuNumber: Int): Int {
        return prefs?.getInt("number$menuNumber", 0)!!
    }

    fun increaseColorIndex(menuNumber: Int) {
        var currentIndex = prefs?.getInt("number$menuNumber", 0)
        currentIndex = currentIndex?.inc()
        prefs?.edit()?.putInt("number$menuNumber", currentIndex!!)?.apply()
    }

    fun resetColorIndex(menuNumber: Int) {
        prefs?.edit()?.putInt("number$menuNumber", 0)?.apply()
    }

}