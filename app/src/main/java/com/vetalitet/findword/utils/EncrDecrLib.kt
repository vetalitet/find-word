package com.vetalitet.findword.utils

import java.io.*
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class EncrDecrLib {

    private val key = byteArrayOf(0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1)
    private val iv = byteArrayOf(1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0)

    var encryptCipher: Cipher? = null
    var decryptCipher: Cipher? = null

    fun main(args: Array<String>) {
        encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        encryptCipher?.init(Cipher.ENCRYPT_MODE, SecretKeySpec( key, "AES"),IvParameterSpec(iv))
        decryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        decryptCipher?.init(Cipher.DECRYPT_MODE, SecretKeySpec( key, "AES"), IvParameterSpec(iv))

        // encrypt
        val encryptedPayloadOutputStream = ByteArrayOutputStream()
        ByteArrayInputStream("secret message".toByteArray()).use({ inputStream -> CipherOutputStream(encryptedPayloadOutputStream, encryptCipher).use({ outputStream -> copy(inputStream, outputStream) }) })
        val encryptedPayload = encryptedPayloadOutputStream.toByteArray()
        println(encryptedPayload.size)
        println(String(encryptedPayload))
        // decrypt
        val decryptedPayloadOutputStream = ByteArrayOutputStream()
        CipherInputStream(ByteArrayInputStream(encryptedPayload), decryptCipher).use({ inputStream -> decryptedPayloadOutputStream.use({ outputStream -> copy(inputStream, outputStream) }) })
        val decryptedPayload = decryptedPayloadOutputStream.toByteArray()
        println(decryptedPayload.size)
        println(String(decryptedPayload))
    }

    @Throws(IOException::class)
    fun copy(input: InputStream, output: OutputStream) {

        //Creating byte array
        val buffer = ByteArray(1024)
        var length = input.read(buffer)

        //Transferring data
        while(length != -1) {
            output.write(buffer, 0, length)
            length = input.read(buffer)
        }

        //Finalizing
        output.flush()
        output.close()
        input.close()
    }

}