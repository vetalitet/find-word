package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.SingleUseCase
import io.reactivex.Single


class FindWordUseCase(private val levelRepository: LevelRepository,
                      threadExecutor: ThreadExecutor,
                      postExecutionThread: PostExecutionThread)
    : SingleUseCase<FindWordUseCase.ReturnObject, FindWordUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Params): Single<ReturnObject> {
        return levelRepository.findWordHint(params.menuNumber)
    }

    data class Params(val menuNumber: Int)

    data class ReturnObject(val word: String, val positions: List<Pair<Int, Int>>)

}