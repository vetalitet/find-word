package com.vetalitet.findword.domain.domain.menuItem.repository.level

import com.vetalitet.findword.data.objects.ChartSizeData
import com.vetalitet.findword.data.objects.ItemData
import com.vetalitet.findword.data.objects.WordData
import com.vetalitet.findword.domain.interactor.all.FindWordUseCase
import io.reactivex.Completable
import io.reactivex.Single

interface LevelDataStore {

    fun getItemDataList(menuNumber: Int): Single<Pair<Int?, List<WordData>>>
    fun getLevelTableSize(menuNumber: Int, levelId: Int): Single<ChartSizeData>
    fun updateCharStatusUseCase(menuItemNumber: Int, levelNumber: Int, wordNumber: Int, status: Int, color: Int): Completable
    fun increaseScore(scoreValue: Int): Single<Int>
    fun increaseCoins(scoreValue: Int): Single<Int>
    fun getScore(): Single<Int>
    fun getCoins(): Single<Int>
    fun checkAllCharsSelected(menuNumber: Int, levelId: Int): Single<Boolean>
    fun findLetterHint(menuNumber: Int): Single<Pair<Int, Int>>
    fun findWordHint(menuNumber: Int): Single<FindWordUseCase.ReturnObject>
    fun checkCoinsAvailability(coins: Int): Single<Boolean>

}
