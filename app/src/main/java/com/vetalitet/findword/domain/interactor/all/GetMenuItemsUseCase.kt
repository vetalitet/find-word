package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.model.MenuItem
import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.SingleUseCase
import io.reactivex.Single

class GetMenuItemsUseCase(private val menuItemRepository: MenuItemRepository,
                          threadExecutor: ThreadExecutor,
                          postExecutionThread: PostExecutionThread)
    : SingleUseCase<List<MenuItem>, GetMenuItemsUseCase.EmptyParams>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: EmptyParams): Single<List<MenuItem>> {
        return menuItemRepository.allMenuItems
    }

    object EmptyParams

}