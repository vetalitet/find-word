package com.vetalitet.findword.domain.domain.menuItem.repository.menu

import com.vetalitet.findword.data.objects.MenuItemData
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItem
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItemStatus
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface MenuItemDataStore {

    fun add(menuItem: MenuItemData): Single<MenuItemData>
    fun addAll(menuItems: List<MenuItemData>): Single<List<MenuItemData>>
    val allMenuItems: Single<List<MenuItem>>

    fun isMenuSaved(menuNumber: Int): Single<Boolean>
    fun updateMenuStatus(menuNumber: Int, menuItemStatus: MenuItemStatus)
    fun increaseCurrentLevel(menuNumber: Int): Completable
    fun finishMenuItem(menuNumber: Int): Completable

}
