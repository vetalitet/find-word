package com.vetalitet.findword.domain.domain.storage.preferences

interface Preferences {
    companion object {
        val preferencesName = "app_prefs"
    }

    fun reset()

    var masterPassword: String?

}
