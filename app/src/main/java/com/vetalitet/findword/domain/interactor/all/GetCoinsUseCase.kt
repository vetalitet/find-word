package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.SingleUseCase
import io.reactivex.Single

class GetCoinsUseCase(private val levelRepository: LevelRepository,
                      threadExecutor: ThreadExecutor,
                      postExecutionThread: PostExecutionThread)
    : SingleUseCase<Int, GetCoinsUseCase.Empty>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Empty): Single<Int> {
        return levelRepository.getCoins()
    }

    object Empty

}
