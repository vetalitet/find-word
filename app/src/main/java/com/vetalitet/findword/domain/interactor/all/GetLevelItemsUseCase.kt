package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.data.objects.LevelData
import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.SingleUseCase
import com.vetalitet.findword.domain.interactor.all.GetLevelItemsUseCase.Params
import io.reactivex.Single

class GetLevelItemsUseCase(private val levelRepository: LevelRepository,
                           threadExecutor: ThreadExecutor,
                           postExecutionThread: PostExecutionThread)
    : SingleUseCase<LevelData, Params>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Params): Single<LevelData> {
        return levelRepository.getItemDataList(params.menuNumber)
                .flatMap({
                    val levelTableSize = levelRepository.getLevelTableSize(params.menuNumber, it.first!!)
                    val levelData = LevelData(levelTableSize.blockingGet(), it.first!!, params.menuNumber, it.second)
                    Single.just(levelData)
                })
    }

    data class Params(val menuNumber: Int)

}