package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.SingleUseCase
import io.reactivex.Single

class CheckAllCharsSelectedUseCase(private val levelRepository: LevelRepository,
                                   threadExecutor: ThreadExecutor,
                                   postExecutionThread: PostExecutionThread)
    : SingleUseCase<Boolean, CheckAllCharsSelectedUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Params): Single<Boolean> {
        return levelRepository.checkAllCharsSelected(params.menuNumber, params.levelId)
    }

    data class Params(val menuNumber: Int, val levelId: Int)

}
