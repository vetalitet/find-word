package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.SingleUseCase
import io.reactivex.Single

class IncreaseScoreUseCase(private val levelRepository: LevelRepository,
                           threadExecutor: ThreadExecutor,
                           postExecutionThread: PostExecutionThread)
    : SingleUseCase<Int, IncreaseScoreUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Params): Single<Int> {
        return levelRepository.increaseScore(params.scoreValue)
    }

    data class Params(val scoreValue: Int)

}