package com.vetalitet.findword.domain.interactor.database

import android.content.Context
import com.vetalitet.findword.data.objects.MenuItemData
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItemStatus
import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.CompletableUseCase
import com.vetalitet.findword.utils.reader.FileReader
import io.reactivex.Completable
import io.reactivex.Single

class CopyFileDataToDBUseCase(private val context: Context,
                              private val menuItemRepository: MenuItemRepository,
                              threadExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread)
    : CompletableUseCase<CopyFileDataToDBUseCase.EmptyParams>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: EmptyParams): Completable {
        var firstRun = false
        return Single.fromCallable({ FileReader(context).readData(0) })
                .flatMap {
                    val resultCollection = mutableListOf<MenuItemData>()
                    it.forEach {
                        if (!menuItemRepository.isMenuSaved(it.menuNumber).blockingGet()) {
                            resultCollection.add(it)
                            if (it.menuNumber == 1) {
                                firstRun = true
                            }
                        }
                    }
                    menuItemRepository.addAll(resultCollection)
                }.map {
            if (firstRun) {
                menuItemRepository.updateMenuStatus(1, MenuItemStatus.ENABLED)
            }
        }.toCompletable()
    }

    object EmptyParams

}
