package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.SingleUseCase
import io.reactivex.Single

class CheckCoinsAvailabilityUseCase(private val levelRepository: LevelRepository,
                      threadExecutor: ThreadExecutor,
                      postExecutionThread: PostExecutionThread)
    : SingleUseCase<Boolean, CheckCoinsAvailabilityUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Params): Single<Boolean> {
        return levelRepository.checkCoinsAvailability(params.coins)
    }

    data class Params(val coins: Int)

}
