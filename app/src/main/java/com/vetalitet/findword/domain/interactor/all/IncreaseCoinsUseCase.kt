package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.SingleUseCase
import io.reactivex.Single

class IncreaseCoinsUseCase(private val levelRepository: LevelRepository,
                           threadExecutor: ThreadExecutor,
                           postExecutionThread: PostExecutionThread)
    : SingleUseCase<Int, IncreaseCoinsUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Params): Single<Int> {
        return levelRepository.increaseCoins(params.coinsValue)
    }

    data class Params(val coinsValue: Int)

}
