package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.model.MenuItemStatus
import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.CompletableUseCase
import io.reactivex.Completable

class UpdateMenuItemStatusUseCase(private val menuItemRepository: MenuItemRepository,
                                  threadExecutor: ThreadExecutor,
                                  postExecutionThread: PostExecutionThread)
    : CompletableUseCase<UpdateMenuItemStatusUseCase.Params>(threadExecutor, postExecutionThread) {

    data class Params(val menuItemNumber: Int, val itemStatus: MenuItemStatus)

    override fun buildObservable(params: Params): Completable {
        return Completable.fromCallable {
            menuItemRepository.updateMenuStatus(params.menuItemNumber, params.itemStatus)
        }
    }

}
