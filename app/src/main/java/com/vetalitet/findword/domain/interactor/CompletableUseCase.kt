package com.vetalitet.findword.domain.interactor

import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.schedulers.Schedulers

abstract class CompletableUseCase<in Params> constructor(
        private val threadExecutor: ThreadExecutor,
        private val postExecutionThread: PostExecutionThread) {

    protected abstract fun buildObservable(params: Params): Completable

    open fun execute(params: Params, observer: CompletableObserver) {
        buildObservable(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(observer)
    }

}
