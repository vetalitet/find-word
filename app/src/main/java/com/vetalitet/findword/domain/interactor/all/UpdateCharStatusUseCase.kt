package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.data.components.RectStatus
import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.CompletableUseCase
import io.reactivex.Completable

class UpdateCharStatusUseCase(private val levelRepository: LevelRepository,
                              threadExecutor: ThreadExecutor,
                              postExecutionThread: PostExecutionThread)
    : CompletableUseCase<UpdateCharStatusUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Params): Completable {
        return levelRepository.updateCharStatusUseCase(params.menuItemNumber, params.levelNumber,
                params.wordNumber, params.status.value, params.color)
    }

    data class Params(val menuItemNumber: Int, val levelNumber: Int,
                      val wordNumber: Int, val status: RectStatus, val color: Int)

}