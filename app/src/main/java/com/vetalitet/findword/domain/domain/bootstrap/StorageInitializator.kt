package com.vetalitet.findword.domain.domain.bootstrap

import com.vetalitet.findword.domain.domain.storage.database.DatabaseConfigurator
import com.vetalitet.findword.domain.domain.storage.preferences.Preferences
import javax.inject.Inject

class StorageInitializator @Inject constructor(val preferences: Preferences, val databaseConfigurator: DatabaseConfigurator) {

    fun configureStorage() {
        preferences.masterPassword?.let {
            databaseConfigurator.configure(it)
        }
    }

}