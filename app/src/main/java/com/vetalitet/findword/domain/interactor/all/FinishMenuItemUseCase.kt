package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.CompletableUseCase
import io.reactivex.Completable

class FinishMenuItemUseCase(private val menuItemRepository: MenuItemRepository,
                            threadExecutor: ThreadExecutor,
                            postExecutionThread: PostExecutionThread)
    : CompletableUseCase<FinishMenuItemUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Params): Completable {
        return menuItemRepository.finishMenuItem(params.menuNumber)
    }

    data class Params(val menuNumber: Int)

}
