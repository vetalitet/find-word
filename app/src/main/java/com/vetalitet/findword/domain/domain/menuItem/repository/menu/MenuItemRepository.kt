package com.vetalitet.findword.domain.domain.menuItem.repository.menu

import com.vetalitet.findword.data.objects.MenuItemData
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItem
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItemStatus
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class MenuItemRepository @Inject constructor(private val menuItemDataStore: MenuItemDataStore) {

    fun add(menuItem: MenuItemData) : Single<MenuItemData> {
        return menuItemDataStore.add(menuItem)
    }

    fun addAll(menuItems: List<MenuItemData>) : Single<List<MenuItemData>> {
        return menuItemDataStore.addAll(menuItems)
    }

    val allMenuItems: Single<List<MenuItem>>
        get() = menuItemDataStore.allMenuItems

    fun isMenuSaved(menuNumber: Int): Single<Boolean> {
        return menuItemDataStore.isMenuSaved(menuNumber)
    }

    fun updateMenuStatus(menuNumber: Int, menuItemStatus: MenuItemStatus) {
        menuItemDataStore.updateMenuStatus(menuNumber, menuItemStatus)
    }

    fun increaseCurrentLevel(menuNumber: Int): Completable {
        return menuItemDataStore.increaseCurrentLevel(menuNumber)
    }

    fun finishMenuItem(menuNumber: Int): Completable {
        return menuItemDataStore.finishMenuItem(menuNumber)
    }

}
