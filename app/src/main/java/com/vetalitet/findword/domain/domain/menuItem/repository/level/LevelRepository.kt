package com.vetalitet.findword.domain.domain.menuItem.repository.level

import com.vetalitet.findword.data.objects.ChartSizeData
import com.vetalitet.findword.data.objects.WordData
import com.vetalitet.findword.domain.interactor.all.FindWordUseCase
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class LevelRepository @Inject constructor(private val levelDataStore: LevelDataStore) {

    fun getItemDataList(menuNumber: Int): Single<Pair<Int?, List<WordData>>> {
        return levelDataStore.getItemDataList(menuNumber)
    }

    fun getLevelTableSize(menuNumber: Int, levelId: Int): Single<ChartSizeData> {
        return levelDataStore.getLevelTableSize(menuNumber, levelId)
    }

    fun updateCharStatusUseCase(menuItemNumber: Int, levelNumber: Int, wordNumber: Int, value: Int, color: Int): Completable {
        return levelDataStore.updateCharStatusUseCase(menuItemNumber, levelNumber, wordNumber, value, color)
    }

    fun increaseScore(scoreValue: Int): Single<Int> {
        return levelDataStore.increaseScore(scoreValue)
    }

    fun increaseCoins(scoreValue: Int): Single<Int> {
        return levelDataStore.increaseCoins(scoreValue)
    }

    fun getScore(): Single<Int> {
        return levelDataStore.getScore()
    }

    fun getCoins(): Single<Int> {
        return levelDataStore.getCoins()
    }

    fun checkAllCharsSelected(menuNumber: Int, levelId: Int): Single<Boolean> {
        return levelDataStore.checkAllCharsSelected(menuNumber, levelId)
    }

    fun findLeterHint(menuNumber: Int): Single<Pair<Int, Int>> {
        return levelDataStore.findLetterHint(menuNumber)
    }

    fun checkCoinsAvailability(coins: Int): Single<Boolean> {
        return levelDataStore.checkCoinsAvailability(coins)
    }

    fun findWordHint(menuNumber: Int): Single<FindWordUseCase.ReturnObject> {
        return levelDataStore.findWordHint(menuNumber)
    }

}
