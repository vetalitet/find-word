package com.vetalitet.findword.domain.domain.storage.database

interface DatabaseConfigurator {
    fun configure(passwordInBase64: String)
}
