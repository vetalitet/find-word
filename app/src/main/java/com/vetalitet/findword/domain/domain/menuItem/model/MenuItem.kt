package com.vetalitet.findword.domain.domain.menuItem.model

data class MenuItem(
        val menuNumber: Int,
        val menuName: String,
        var menuStatus: MenuItemStatus
)

enum class MenuItemStatus(open var value: Int) {
    DISABLED(0), ENABLED(1), COMPLETED(2);
}