package com.vetalitet.findword.domain.interactor.all

import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.SingleUseCase
import io.reactivex.Single

class FindHintUseCase(private val levelRepository: LevelRepository,
                      threadExecutor: ThreadExecutor,
                      postExecutionThread: PostExecutionThread)
    : SingleUseCase<Pair<Int, Int>, FindHintUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun buildObservable(params: Params): Single<Pair<Int, Int>> {
        return levelRepository.findLeterHint(params.menuNumber)
    }

    data class Params(val menuNumber: Int)

}
