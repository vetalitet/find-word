package com.vetalitet.findword.storage.realm.mapper.level

import com.vetalitet.findword.data.objects.ItemData
import com.vetalitet.findword.mapper.Mapper
import com.vetalitet.findword.storage.realm.model.CharEntity
import javax.inject.Inject

class ItemDataRealmMapper @Inject constructor() : Mapper<ItemData, CharEntity> {

    override fun transform(value: ItemData): CharEntity {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun reverseTransform(value: CharEntity): ItemData {
        return ItemData(value.x, value.y, value.charNumber, value.wordNumber, value.status, value.color, value.charac[0])
    }

}
