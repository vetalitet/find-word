package com.vetalitet.findword.storage.realm.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class LevelEntity: RealmObject() {

    @PrimaryKey
    open var id: Int = 0
    open var levelNumber: Int = 0
    open var menuNumber: Int = 0

    open var width: Int = 0
    open var height: Int = 0

}
