package com.vetalitet.findword.storage

import com.vetalitet.findword.components.WordTable
import com.vetalitet.findword.data.components.RectStatus
import com.vetalitet.findword.data.objects.ChartSizeData
import com.vetalitet.findword.data.objects.ItemData
import com.vetalitet.findword.data.objects.WordData
import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelDataStore
import com.vetalitet.findword.domain.interactor.all.FindWordUseCase
import com.vetalitet.findword.storage.realm.mapper.level.ItemDataRealmMapper
import com.vetalitet.findword.storage.realm.model.CharEntity
import com.vetalitet.findword.storage.realm.model.GameEntity
import com.vetalitet.findword.storage.realm.model.LevelEntity
import com.vetalitet.findword.storage.realm.model.MenuItemEntity
import io.reactivex.Completable
import io.reactivex.Single
import io.realm.Realm
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class LevelDiskDataStorage @Inject constructor(private val itemDataRealmMapper: ItemDataRealmMapper) : LevelDataStore {

    override fun getItemDataList(menuNumber: Int): Single<Pair<Int?, List<WordData>>> {
        return Single.fromCallable {
            Realm.getDefaultInstance().use {
                val realmCurrentLevel = it.where(MenuItemEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .findFirst()
                val realmResults = it.where(CharEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .equalTo("levelNumber", realmCurrentLevel?.currentLevel)
                        .findAll()
                val reverseTransform = itemDataRealmMapper.reverseTransform(realmResults)
                val map = mutableMapOf<Int, List<ItemData>>()
                reverseTransform.forEach {
                    if (map[it.wordNumber] == null) {
                        val list = ArrayList<ItemData>()
                        list.add(it)
                        map.put(it.wordNumber, list)
                    } else {
                        val list: MutableList<ItemData> = map[it.wordNumber] as MutableList<ItemData>
                        list.add(it)
                        map.put(it.wordNumber, list)
                    }
                }
                val result = ArrayList<WordData>()
                map.values.forEach {
                    result.add(WordData(it))
                }
                Pair<Int?, List<WordData>>(realmCurrentLevel?.currentLevel, result)
            }
        }
    }

    override fun getLevelTableSize(menuNumber: Int, levelId: Int): Single<ChartSizeData> {
        return Single.fromCallable {
            Realm.getDefaultInstance().use {
                val realmResult = it.where(LevelEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .equalTo("levelNumber", levelId)
                        .findFirst()
                ChartSizeData(realmResult?.width!!, realmResult.height)
            }
        }
    }

    override fun updateCharStatusUseCase(menuItemNumber: Int, levelNumber: Int, wordNumber: Int, status: Int, color: Int): Completable {
        return Completable.fromCallable {
            Realm.getDefaultInstance().use {
                val realmResult = it.where(CharEntity::class.java)
                        .equalTo("menuNumber", menuItemNumber)
                        .equalTo("levelNumber", levelNumber)
                        .equalTo("wordNumber", wordNumber)
                        .findAll()
                it.beginTransaction()
                realmResult.forEach {
                    it.status = status
                    it.color = color
                }
                it.commitTransaction()

                val selectingResult = it.where(CharEntity::class.java)
                        .equalTo("menuNumber", menuItemNumber)
                        .equalTo("levelNumber", levelNumber)
                        .equalTo("wordNumber", wordNumber)
                        .equalTo("status", RectStatus.IsSelecting.value)
                        .findAll()

                it.beginTransaction()
                selectingResult.forEach {
                    it.status = RectStatus.None.value
                }
                it.commitTransaction()
            }
        }
    }

    override fun increaseScore(scoreValue: Int): Single<Int> {
        return Single.fromCallable {
            Realm.getDefaultInstance().use {
                val realmResult = it.where(GameEntity::class.java).findFirst()
                if (realmResult == null) {
                    it.beginTransaction()
                    it.createObject(GameEntity::class.java)
                    it.commitTransaction()
                }
                val realmResult1 = it.where(GameEntity::class.java).findFirst()
                it.beginTransaction()
                var score = realmResult1?.score ?: 0
                score = score.plus(scoreValue)
                realmResult1?.score = score
                it.commitTransaction()
                score
            }
        }
    }

    private fun createGameObjectIfEmpty() {
        Realm.getDefaultInstance().use {
            val realmResult = it.where(GameEntity::class.java).findFirst()
            if (realmResult == null) {
                it.beginTransaction()
                val createObject = it.createObject(GameEntity::class.java)
                createObject.coins = 30
                it.commitTransaction()
            }
        }
    }

    override fun increaseCoins(scoreValue: Int): Single<Int> {
        return Single.fromCallable {
            createGameObjectIfEmpty()
            Realm.getDefaultInstance().use {
                val realmResult1 = it.where(GameEntity::class.java).findFirst()
                it.beginTransaction()
                var coins = realmResult1?.coins ?: 0
                coins = coins.plus(scoreValue)
                realmResult1?.coins = coins
                it.commitTransaction()
                coins
            }
        }
    }

    override fun getScore(): Single<Int> {
        return Single.fromCallable {
            createGameObjectIfEmpty()
            Realm.getDefaultInstance().use {
                val realmResult = it.where(GameEntity::class.java).findFirst()
                realmResult?.score ?: 0
            }
        }
    }

    override fun getCoins(): Single<Int> {
        return Single.fromCallable {
            createGameObjectIfEmpty()
            Realm.getDefaultInstance().use {
                val realmResult = it.where(GameEntity::class.java).findFirst()
                realmResult?.coins ?: 0
            }
        }
    }

    override fun checkAllCharsSelected(menuNumber: Int, levelId: Int): Single<Boolean> {
        return Single.fromCallable {
            Realm.getDefaultInstance().use {
                val realmResult = it.where(CharEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .equalTo("levelNumber", levelId)
                        .equalTo("status", RectStatus.None.value)
                        .findAll()
                realmResult.isEmpty() // if null - all rows are already selected
            }
        }
    }

    override fun findLetterHint(menuNumber: Int): Single<Pair<Int, Int>> {
        return Single.fromCallable {
            Realm.getDefaultInstance().use {
                val menuItem = it.where(MenuItemEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .findFirst()
                val firstChars = it.where(CharEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .equalTo("levelNumber", menuItem?.currentLevel)
                        .equalTo("index", 1L)
                        .equalTo("status", 0L)
                        .findAll()
                val charEntity = firstChars[Random().nextInt(firstChars.size)]
                Pair(charEntity?.x!!, charEntity.y)
            }
        }
    }

    override fun findWordHint(menuNumber: Int): Single<FindWordUseCase.ReturnObject> {
        return Single.fromCallable {
            Realm.getDefaultInstance().use {
                val menuItem = it.where(MenuItemEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .findFirst()
                val firstChars = it.where(CharEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .equalTo("levelNumber", menuItem?.currentLevel)
                        .equalTo("index", 1L)
                        .equalTo("status", 0L)
                        .findAll()
                val charEntity = firstChars[Random().nextInt(firstChars.size)]
                val allLetters = it.where(CharEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .equalTo("levelNumber", menuItem?.currentLevel)
                        .equalTo("wordNumber", charEntity?.wordNumber)
                        .equalTo("status", 1L)
                        .findAll()

                val realmResult = it.where(CharEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .equalTo("levelNumber", menuItem?.currentLevel)
                        .equalTo("wordNumber", charEntity?.wordNumber)
                        .findAll()
                it.beginTransaction()
                val generateColor = WordTable.generateColor(menuNumber)
                realmResult.forEach {
                    it.status = RectStatus.Selected.value
                    it.color = generateColor
                }
                it.commitTransaction()

                val result = ArrayList<Pair<Int, Int>>()
                var word: String = ""
                allLetters.forEach {
                    word = word.plus(it.charac)
                    result.add(Pair(it.x, it.y))
                }
                FindWordUseCase.ReturnObject(word, result)
            }
        }
    }

    override fun checkCoinsAvailability(coins: Int): Single<Boolean> {
        return Single.fromCallable {
            createGameObjectIfEmpty()
            Realm.getDefaultInstance().use {
                val realmResult = it.where(GameEntity::class.java).findFirst()
                realmResult?.coins!! >= coins
            }
        }
    }

}
