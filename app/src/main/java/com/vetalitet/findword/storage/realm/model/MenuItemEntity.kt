package com.vetalitet.findword.storage.realm.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class MenuItemEntity : RealmObject() {

    @PrimaryKey
    open var id: Int = 0
    open var menuNumber: Int = 0
    open var currentLevel: Int = 1
    open var menuName: String = ""
    open var menuStatus: Int = 0

}
