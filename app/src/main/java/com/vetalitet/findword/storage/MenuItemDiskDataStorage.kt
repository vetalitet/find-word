package com.vetalitet.findword.storage

import com.vetalitet.findword.data.objects.MenuItemData
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItem
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItemStatus
import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemDataStore
import com.vetalitet.findword.storage.realm.mapper.menu.MenuItemRealmMapper
import com.vetalitet.findword.storage.realm.model.CharEntity
import com.vetalitet.findword.storage.realm.model.LevelEntity
import com.vetalitet.findword.storage.realm.model.MenuItemEntity
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.processors.BehaviorProcessor
import io.realm.Realm
import javax.inject.Inject

class MenuItemDiskDataStorage @Inject constructor(private val menuItemRealmMapper: MenuItemRealmMapper) : MenuItemDataStore {

    private val changesPublishSubject = BehaviorProcessor.create<Unit>().toSerialized()

    override val allMenuItems: Single<List<MenuItem>>
        get() = Single.just(menuItemsAsBlocking)
    //get() = changesPublishSubject.map { menuItemsAsBlocking }.startWith(Flowable.just(menuItemsAsBlocking))

    private val menuItemsAsBlocking: List<MenuItem>
        get() {
            Realm.getDefaultInstance().use {
                val realmResults = it.where(MenuItemEntity::class.java).findAll()
                return menuItemRealmMapper.reverseTransform(realmResults)
            }
        }

    override fun add(menuItem: MenuItemData): Single<MenuItemData> {
        return Single.fromCallable {
            storeMenuItem(menuItem.menuNumber, menuItem.name)
            menuItem.levels.map {
                val level = it
                storeLevel(level.levelNumber, menuItem.menuNumber, level.chartSizeData.x, level.chartSizeData.y)
                it.words.map {
                    val word = it
                    var index = 1
                    it.items.map {
                        val item = it
                        storeCharacter(level.levelNumber, menuItem.menuNumber, item.charNumber, item.wordNumber, item.x, item.y, index, item.char, item.color)
                        index++
                    }
                }
            }

            menuItem
        }
    }

    override fun isMenuSaved(menuNumber: Int): Single<Boolean> {
        Realm.getDefaultInstance().use {
            val realmResults = it.where(MenuItemEntity::class.java).equalTo("menuNumber", menuNumber).findAll()
            return Single.just(realmResults.isNotEmpty())
        }
    }

    override fun updateMenuStatus(menuNumber: Int, menuItemStatus: MenuItemStatus) {
        Realm.getDefaultInstance().use {
            val realmResult = it.where(MenuItemEntity::class.java).equalTo("menuNumber", menuNumber).findFirst()
            it.beginTransaction()
            realmResult?.menuStatus = menuItemStatus.value
            it.copyToRealmOrUpdate(realmResult)
            it.commitTransaction()
        }
    }

    override fun addAll(menuItems: List<MenuItemData>): Single<List<MenuItemData>> {
        return Single.fromCallable {
            menuItems.map {
                val menuItem = it
                storeMenuItem(menuItem.menuNumber, menuItem.name)
                val characters : MutableList<CharEntity> = ArrayList()
                it.levels.map {
                    val level = it
                    storeLevel(level.levelNumber, menuItem.menuNumber, level.chartSizeData.x, level.chartSizeData.y)
                    it.words.map {
                        val word = it
                        var index = 1
                        it.items.map {
                            val item = it
                            val charEntity = prepateCharacter(level.levelNumber, menuItem.menuNumber, item.charNumber,
                                    item.wordNumber, item.x, item.y, index, item.char, item.color)
                            characters.add(charEntity)
                            index++
                        }
                    }
                }
                storeCharacters(characters)
            }

            menuItems
        }
    }

    private fun prepateCharacter(levelNumber: Int, menuNumber: Int, charNumber: Int, wordNumber: Int,
                                 x: Int, y: Int, index: Int, char: Char, color: Int): CharEntity {
        val charEntity = CharEntity()
        charEntity.menuNumber = menuNumber
        charEntity.levelNumber = levelNumber
        charEntity.charNumber = charNumber
        charEntity.wordNumber = wordNumber
        charEntity.x = x
        charEntity.y = y
        charEntity.index = index
        charEntity.color = color
        charEntity.charac = char.toString()
        return charEntity
    }

    private fun storeMenuItem(menuNumber: Int, name: String) {
        val menuItemEntity = MenuItemEntity()
        menuItemEntity.menuNumber = menuNumber
        menuItemEntity.menuName = name

        Realm.getDefaultInstance().use {
            it.executeTransaction {
                val currentMaxId = it.where(MenuItemEntity::class.java).max("id")
                val nextId: Int
                if (currentMaxId == null) {
                    nextId = 1
                } else {
                    nextId = currentMaxId.toInt() + 1
                }
                menuItemEntity.id = nextId
                it.copyToRealm(menuItemEntity)
            }
        }
    }

    private fun storeLevel(levelNumber: Int, menuNumber: Int, x: Int, y: Int) {
        val levelEntity = LevelEntity()
        levelEntity.menuNumber = menuNumber
        levelEntity.levelNumber = levelNumber
        levelEntity.width = x
        levelEntity.height = y

        Realm.getDefaultInstance().use {
            it.executeTransaction {
                val currentMaxId = it.where(LevelEntity::class.java).max("id")
                val nextId: Int
                if (currentMaxId == null) {
                    nextId = 1
                } else {
                    nextId = currentMaxId.toInt() + 1
                }
                levelEntity.id = nextId
                it.copyToRealm(levelEntity)
            }
        }
    }

    private fun storeCharacter(levelNumber: Int, menuNumber: Int, charNumber: Int, wordNumber: Int, x: Int, y: Int, index: Int, char: Char, color: Int) {
        val charEntity = CharEntity()
        charEntity.menuNumber = menuNumber
        charEntity.levelNumber = levelNumber
        charEntity.charNumber = charNumber
        charEntity.wordNumber = wordNumber
        charEntity.x = x
        charEntity.y = y
        charEntity.index = index
        charEntity.color = color
        charEntity.charac = char.toString()

        Realm.getDefaultInstance().use {
            it.beginTransaction()
            val currentMaxId = it.where(CharEntity::class.java).max("id")
            val nextId: Int
            if (currentMaxId == null) {
                nextId = 1
            } else {
                nextId = currentMaxId.toInt() + 1
            }
            charEntity.id = nextId
            it.copyToRealm(charEntity)
            it.commitTransaction()
        }
    }

    private fun storeCharacters(charEntities: Iterable<CharEntity>) {
        Realm.getDefaultInstance().use {
            it.beginTransaction()
            val currentMaxId = it.where(CharEntity::class.java).max("id")
            var nextId: Int
            if (currentMaxId == null) {
                nextId = 1
            } else {
                nextId = currentMaxId.toInt() + 1
            }
            charEntities.forEach {
                it.id = nextId
                nextId++
            }
            it.copyToRealm(charEntities)
            it.commitTransaction()
        }
    }

    override fun increaseCurrentLevel(menuNumber: Int): Completable {
        return Completable.fromCallable {
            Realm.getDefaultInstance().use {
                val menuItem = it.where(MenuItemEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .findFirst()
                if (menuItem != null) {
                    it.beginTransaction()
                    menuItem.currentLevel = menuItem.currentLevel.plus(1)
                    it.commitTransaction()
                }
            }
        }
    }

    override fun finishMenuItem(menuNumber: Int): Completable {
        return Completable.fromCallable {
            Realm.getDefaultInstance().use {
                val menuItem = it.where(MenuItemEntity::class.java)
                        .equalTo("menuNumber", menuNumber)
                        .findFirst()
                if (menuItem != null) {
                    it.beginTransaction()
                    menuItem.menuStatus = MenuItemStatus.COMPLETED.value
                    it.commitTransaction()
                }

                val nextMenuItem = it.where(MenuItemEntity::class.java)
                        .equalTo("menuNumber", menuNumber + 1)
                        .notEqualTo("menuStatus", MenuItemStatus.COMPLETED.value)
                        .findFirst()

                val enabledBeforeMenuNumber = it.where(MenuItemEntity::class.java)
                        .lessThan("menuNumber", menuNumber)
                        .notEqualTo("menuStatus", MenuItemStatus.ENABLED.value)
                        .findFirst()

                if (nextMenuItem != null && enabledBeforeMenuNumber == null) {
                    it.beginTransaction()
                    nextMenuItem.menuStatus = MenuItemStatus.ENABLED.value
                    it.commitTransaction()
                }
            }
        }
    }

}
