package com.vetalitet.findword.storage.realm.mapper.menu

import com.vetalitet.findword.domain.domain.menuItem.model.MenuItem
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItemStatus
import com.vetalitet.findword.mapper.Mapper
import com.vetalitet.findword.storage.realm.model.MenuItemEntity
import javax.inject.Inject

class MenuItemRealmMapper @Inject constructor(): Mapper<MenuItem, MenuItemEntity> {

    override fun transform(value: MenuItem): MenuItemEntity {
        throw UnsupportedOperationException("Not implemented")
    }

    override fun reverseTransform(value: MenuItemEntity): MenuItem {
        return MenuItem(value.menuNumber, value.menuName, getMenuItemStatus(value.menuStatus))
    }

    private fun getMenuItemStatus(status: Int): MenuItemStatus {
        return when(status) {
            MenuItemStatus.ENABLED.value -> MenuItemStatus.ENABLED
            MenuItemStatus.COMPLETED.value -> MenuItemStatus.COMPLETED
            else -> MenuItemStatus.DISABLED
        }
    }

}
