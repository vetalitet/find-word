package com.vetalitet.findword.storage.realm.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class CharEntity : RealmObject() {

    @PrimaryKey
    open var id: Int = 0

    open var charNumber: Int = 0
    open var menuNumber: Int = 0
    open var levelNumber: Int = 0
    open var wordNumber: Int = 0
    open var charac: String = ""

    open var x: Int = 0
    open var y: Int = 0
    open var status: Int = 0
    open var color: Int = 0
    open var index: Int = 0

}
