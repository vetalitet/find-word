package com.vetalitet.findword.storage.realm.model

import io.realm.RealmObject

open class GameEntity: RealmObject() {

    open var score: Int = 0
    open var coins: Int = 0

}
