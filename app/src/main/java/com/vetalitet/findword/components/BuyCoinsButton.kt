package com.vetalitet.findword.components

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.vetalitet.findword.R
import kotlinx.android.synthetic.main.buy_coins_button.view.*

class BuyCoinsButton : LinearLayout {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init()
        initAttributes(context, attributeSet)
    }

    private fun initAttributes(context: Context, attributeSet: AttributeSet) {
        val array = context.obtainStyledAttributes(attributeSet, R.styleable.BuyCoinsButton)
        val coinsQuantity = array.getString(R.styleable.BuyCoinsButton_coins_quantity)
        val coinsPrice = array.getString(R.styleable.BuyCoinsButton_coins_price)
        val coinsImage = array.getResourceId(R.styleable.BuyCoinsButton_coins_image, 0)
        val coinsMarginInside = array.getDimensionPixelSize(R.styleable.BuyCoinsButton_coins_margin_inside, 0)

        tvCoinsQuantity.text = coinsQuantity
        tvCoinsPrice.text = coinsPrice
        iCoinsImage.setImageResource(coinsImage)

        val params = iCoinsImage.layoutParams as MarginLayoutParams
        params.leftMargin = coinsMarginInside
        params.rightMargin = coinsMarginInside
        params.topMargin = coinsMarginInside
        params.bottomMargin = coinsMarginInside
        iCoinsImage.layoutParams = params

        array.recycle()
    }

    private fun init() {
        inflate(context, R.layout.buy_coins_button, this)
    }

}