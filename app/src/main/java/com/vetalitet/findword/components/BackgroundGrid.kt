package com.vetalitet.findword.components

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.util.AttributeSet
import android.view.ViewParent
import android.widget.LinearLayout
import android.view.ViewGroup
import com.vetalitet.findword.R

class BackgroundGrid : LinearLayout {

    private val bgPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val gridLinePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var wordTable: WordTable? = null
    private val horizontalPoints : MutableList<Pair<PointF, PointF>> = ArrayList()
    private val verticalPoints : MutableList<Pair<PointF, PointF>> = ArrayList()

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init()
    }

    private fun init() {
        bgPaint.color = context.resources.getColor(android.R.color.transparent)

        gridLinePaint.strokeWidth = 3f
        gridLinePaint.color = context.resources.getColor(android.R.color.white)

        setWillNotDraw(false)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        var firstLastLine = 0
        val size = Math.min(measuredWidth, measuredHeight)

        if (wordTable != null) {
            firstLastLine = (wordTable?.x?.div(wordTable?.getItemSize()!!)!!.toInt() + 1)
            if (firstLastLine == 1) firstLastLine++
        }

        var height = wordTable?.measuredHeight!! * 0.95
        height += wordTable?.getItemSize()!! * firstLastLine
        setMeasuredDimension(size, height.toInt())

        if (wordTable != null) {
            val data = wordTable?.getData()
            if (data != null) {
                wordTable?.x = ((size - wordTable?.width!!) / 2).toFloat()
                wordTable?.y = ((height - wordTable?.width!!) / 2).toFloat()
                prepareGridPaintingData(wordTable?.x!!, wordTable?.y!!, wordTable?.getItemSize()!!, data.chartSizeData.x)
            }
        }
    }

    private fun prepareGridPaintingData(posX: Float, posY: Float, itemSize: Int, itemsCount: Int) {
        verticalPoints.clear()
        horizontalPoints.clear()

        val spaceBetween = resources.getInteger(R.integer.space_between)

        for (i in 1 until itemsCount + 2) {
            when (i) {
                1 -> {
                    val hp1 = PointF(0f, posY + spaceBetween)
                    val hp2 = PointF(measuredWidth.toFloat(), posY + spaceBetween)
                    horizontalPoints.add(Pair(hp1, hp2))

                    val vp1 = PointF(posX + spaceBetween, measuredHeight.toFloat())
                    val vp2 = PointF(posX + spaceBetween, 0f)
                    verticalPoints.add(Pair(vp1, vp2))
                }
                itemsCount + 1 -> {
                    val hp1 = PointF(0f, (posY + itemSize * (i - 1)) - spaceBetween)
                    val hp2 = PointF(measuredWidth.toFloat(), (posY + itemSize * (i - 1)) - spaceBetween)
                    horizontalPoints.add(Pair(hp1, hp2))

                    val vp1 = PointF((posX + itemSize * (i - 1)) - spaceBetween, measuredHeight.toFloat())
                    val vp2 = PointF((posX + itemSize * (i - 1)) - spaceBetween, 0f )
                    verticalPoints.add(Pair(vp1, vp2))
                }
                else -> {
                    val hp1 = PointF(0f, (posY + itemSize * (i - 1)) - spaceBetween)
                    val hp2 = PointF(measuredWidth.toFloat(), (posY + itemSize * (i - 1)) - spaceBetween)
                    horizontalPoints.add(Pair(hp1, hp2))

                    val hp3 = PointF(0f, (posY + itemSize * (i - 1)) + spaceBetween)
                    val hp4 = PointF(measuredWidth.toFloat(), (posY + itemSize * (i - 1)) + spaceBetween)
                    horizontalPoints.add(Pair(hp3, hp4))

                    val vp1 = PointF((posX + itemSize * (i - 1)) - spaceBetween, measuredHeight.toFloat())
                    val vp2 = PointF((posX + itemSize * (i - 1)) - spaceBetween, 0f )
                    verticalPoints.add(Pair(vp1, vp2))

                    val vp3 = PointF((posX + itemSize * (i - 1)) + spaceBetween, measuredHeight.toFloat())
                    val vp4 = PointF((posX + itemSize * (i - 1)) + spaceBetween, 0f)
                    verticalPoints.add(Pair(vp3, vp4))
                }
            }
        }

        var i = posX - itemSize + spaceBetween + spaceBetween / 2
        while (i > 0) {
            val vp1 = PointF(i, 0f)
            val vp2 = PointF(i, measuredHeight.toFloat())
            verticalPoints.add(Pair(vp1, vp2))
            i = i.minus(itemSize)
        }

        var i1 = posX + wordTable?.width!! + itemSize - spaceBetween - spaceBetween / 2
        while (i1 < measuredWidth) {
            val vp1 = PointF(i1, 0f)
            val vp2 = PointF(i1, measuredHeight.toFloat())
            verticalPoints.add(Pair(vp1, vp2))
            i1 = i1.plus(itemSize)
        }

        var i2 = posY - itemSize + spaceBetween + spaceBetween * 2
        while (i2 > itemSize / 2) {
            val vp1 = PointF(0f, i2)
            val vp2 = PointF(measuredWidth.toFloat(), i2)
            horizontalPoints.add(Pair(vp1, vp2))
            i2 = i2.minus(itemSize)
        }

        var i3 = posY + wordTable?.height!! + itemSize - spaceBetween * 3
        while (i3 < measuredHeight - itemSize / 2) {
            val vp1 = PointF(0f, i3)
            val vp2 = PointF(measuredWidth.toFloat(), i3)
            verticalPoints.add(Pair(vp1, vp2))
            i3 = i3.plus(itemSize)
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (wordTable == null) return

        canvas.drawRect(0f, 0f, measuredWidth.toFloat(), measuredHeight.toFloat(), bgPaint)

        horizontalPoints.forEach {
            canvas.drawLine(it.first.x, it.first.y, it.second.x, it.second.y, gridLinePaint)
        }

        verticalPoints.forEach {
            canvas.drawLine(it.first.x, it.first.y, it.second.x, it.second.y, gridLinePaint)
        }

        canvas.drawLine(0f, 0f, measuredWidth.toFloat(), 0f, gridLinePaint)
        canvas.drawLine(0f, measuredHeight.toFloat(), measuredWidth.toFloat(), measuredHeight.toFloat(), gridLinePaint)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        findWordTable(parent)
    }

    private fun findWordTable(viewParent: ViewParent) {
        val isValidParent = viewParent is ViewGroup && viewParent.childCount > 0

        if (!isValidParent) {
            return
        }

        val wordTable = (viewParent as ViewGroup).findViewById<WordTable>(R.id.worldTable)
        if (wordTable != null) {
            setWordTable(wordTable)
        } else {
            findWordTable(viewParent.parent)
        }
    }

    private fun setWordTable(wordTable: WordTable) {
        this.wordTable = wordTable
    }

}
