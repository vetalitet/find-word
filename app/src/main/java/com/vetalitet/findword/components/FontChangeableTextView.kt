package com.vetalitet.findword.components

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import com.vetalitet.findword.R
import com.vetalitet.findword.utils.FontUtils

class FontChangeableTextView : TextView {

    constructor(context: Context): super(context)

    constructor(context: Context, attributeSet: AttributeSet): super(context, attributeSet) {
        setCustomFont(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet, defStyle: Int): super(context, attributeSet, defStyle) {
        setCustomFont(context, attributeSet)
    }

    private fun setCustomFont(context: Context, attributeSet: AttributeSet) {
        val array = context.obtainStyledAttributes(attributeSet, R.styleable.FontChangeableTextView)
        val customFont = array.getString(R.styleable.FontChangeableTextView_custom_font)
        typeface = FontUtils.loadCustomFont(context, customFont)
        array.recycle()
    }

}
