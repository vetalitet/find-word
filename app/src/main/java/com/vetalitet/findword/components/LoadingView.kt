package com.vetalitet.findword.components

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.vetalitet.findword.R

class LoadingView: LinearLayout {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init()
    }

    private fun init() {
        inflate(context, R.layout.loading_layout, this)
    }

}
