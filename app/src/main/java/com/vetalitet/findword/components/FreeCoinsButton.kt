package com.vetalitet.findword.components

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.vetalitet.findword.R
import kotlinx.android.synthetic.main.free_coins_button.view.*

class FreeCoinsButton: LinearLayout {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init()
        initAttributes(context, attributeSet)
    }

    private fun initAttributes(context: Context, attributeSet: AttributeSet) {
        val array = context.obtainStyledAttributes(attributeSet, R.styleable.FreeCoinsButton)
        val buttonText = array.getString(R.styleable.FreeCoinsButton_button_text)

        coinText.text = buttonText

        array.recycle()
    }

    private fun init() {
        inflate(context, R.layout.free_coins_button, this)
    }

}