package com.vetalitet.findword.components

import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.vetalitet.findword.data.components.RectItem
import com.vetalitet.findword.data.components.RectStatus
import com.vetalitet.findword.data.objects.LevelData
import com.vetalitet.findword.utils.helpers.SharedPreferencesManager
import java.util.*
import android.graphics.Typeface
import android.util.TypedValue
import com.vetalitet.findword.R
import kotlinx.android.synthetic.main.fragment_main_layout.*


class WordTable : View {

    private var data: LevelData? = null

    private val empty = 0.px
    private val bounds: Rect = Rect()

    private var allEmptyWidth: Int = 0
    private var itemSize = 0
    private var maxIndex = 1
    private var selectedItems: MutableList<RectItem> = ArrayList()
    private var currentSelectedItem: RectItem? = null

    private val rectItems: MutableList<RectItem> = ArrayList()

    private val bgPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val borderPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val itemPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val itemIsSelectingPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val textPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    lateinit var callbackInterface: CallbackInterface

    constructor(ctx: Context) : super(ctx) {
        init(ctx)
    }

    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs) {
        init(ctx)
    }

    private fun init(ctx: Context) {
        bgPaint.setARGB(0, 240, 240, 240)
        itemPaint.setARGB(255, 255, 255, 255)

        borderPaint.strokeWidth = 3f
        borderPaint.style = Paint.Style.STROKE
        borderPaint.setARGB(255, 0, 0, 0)

        val plain = Typeface.createFromAsset(context.assets, "fonts/MuliBold.ttf")
        textPaint.setARGB(211, 22, 50, 76)
        textPaint.typeface = plain
        //textPaint.textSize = 160f

        shpManager = SharedPreferencesManager(ctx)
    }

    fun setData(data: LevelData) {
        this.data = data
        initSHPManager(data)
        handleCorrectWord()
        parent.requestLayout()
        requestLayout()
        invalidate()
    }

    private fun initSHPManager(data: LevelData) {
        val allWithStatusSelected = data.getAllWithStatus(RectStatus.Selected.value)
        if (allWithStatusSelected == 0) {
            shpManager?.resetColorIndex(data.menuItemNumber)
            Collections.shuffle(colorsArray)
        }
    }

    fun getData(): LevelData? {
        return data
    }

    fun getItemSize(): Int {
        return itemSize
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        data?.let {
            allEmptyWidth = empty.times(data!!.chartSizeData.x + 1)
            itemSize = ((width - allEmptyWidth) / data!!.chartSizeData.x) - 1

            rectItems.clear()
            runOverData(data!!, object : RunOverCallback {
                override fun getCoords(x: Int, y: Int) {
                    val left: Float = empty.toFloat() * y + (itemSize * (y - 1)) + 1
                    val top: Float = empty.toFloat() * x + (itemSize * (x - 1)) + 1

                    val char = data!!.getCharBy(y, x)?.toUpperCase()
                    val status = data!!.getStatusBy(y, x)
                    val color = data!!.getColorBy(y, x)

                    val defaultColor: Int = Color.rgb(255, 196, 0)

                    val rect = RectF(left, top, left + itemSize, top + itemSize)
                    val rectItem = RectItem(rect, char, defaultColor, y, x)
                    rectItem.status = getStatusByValue(status)
                    if (Math.abs(color) > 0) {
                        rectItem.color = color
                    }
                    rectItems.add(rectItem)
                }
            })

            when {
                data!!.chartSizeData.x == 7 -> textPaint.textSize = (itemSize * 0.5).toFloat() //70f
                data!!.chartSizeData.x == 6 -> textPaint.textSize = (itemSize * 0.5).toFloat() //80f
                data!!.chartSizeData.x == 5 -> textPaint.textSize = (itemSize * 0.5).toFloat() //100f
                else -> textPaint.textSize = spToPx(context.resources.getDimension(R.dimen.main_word_table_letter_4x4_size)).toFloat() //110f
            }
        }

        val size = Math.min(measuredWidth, measuredHeight)
        setMeasuredDimension(size, size)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawRect(0f, 0f, measuredWidth.toFloat(), measuredHeight.toFloat(), bgPaint)

        for (rectItem in rectItems) {
            itemIsSelectingPaint.color = rectItem.color
            when (rectItem.status) {
                RectStatus.None ->
                    canvas.drawRoundRect(rectItem.rect, 15f, 15f, itemPaint)
                RectStatus.IsSelecting ->
                    canvas.drawRoundRect(rectItem.rect, 15f, 15f, itemIsSelectingPaint)
                RectStatus.Selected ->
                    canvas.drawRoundRect(rectItem.rect, 15f, 15f, itemIsSelectingPaint)
            }
            canvas.drawRoundRect(rectItem.rect, 15f, 15f, borderPaint)

            textPaint.getTextBounds(rectItem.char.toString(), 0, 1, bounds)
            val textX = rectItem.rect.left + itemSize / 2 - bounds.width() / 2
            val textY = rectItem.rect.top + itemSize / 2 + bounds.height() / 2
            canvas.drawText(rectItem.char.toString(), textX - 4, textY, textPaint)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val actionMasked = event.actionMasked
        when (actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                rectItems.filter { it.status == RectStatus.IsSelecting }
                        .forEach {
                            it.status = RectStatus.None
                        }
                rectItems.filter { it.status == RectStatus.None && it.rect.contains(event.x, event.y) }
                        .forEach {
                            it.index = maxIndex
                            it.status = RectStatus.IsSelecting
                            selectedItems.add(it)
                            currentSelectedItem = it
                            currentSelectedItem?.index = maxIndex
                            maxIndex++
                        }
                invalidate()
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                rectItems.filter { it.status != RectStatus.None && it.rect.contains(event.x, event.y) }
                        .forEach { currentSelectedItem = it }

                val intersectRects = rectItems.filter {
                    it.status == RectStatus.None && it.rect.contains(event.x, event.y) && currentSelectedItem?.index == maxIndex - 1
                }
                intersectRects.forEach {
                    it.status = RectStatus.IsSelecting
                    selectedItems.add(it)
                    selectedItems[selectedItems.size - 1].index = maxIndex
                    currentSelectedItem = it
                    maxIndex++
                }
                invalidate()
                return true
            }
            MotionEvent.ACTION_UP -> {
                handleCorrectWord()
                releaseAll()
                invalidate()
            }
        }
        return super.onTouchEvent(event)
    }

    private fun handleCorrectWord() {
        val selectedWord: List<XYChar> = getSelectedWord()
        val matchedWordPair = data!!.checkContainsWord(selectedWord)
        val wordNumber = matchedWordPair.first
        val equalsEnum = matchedWordPair.second
        val newGeneratedColor = generateColor(data!!.menuItemNumber)
        if (equalsEnum == LevelData.EqualsEnum.EQUALS_COMPLETELY) {
            rectItems.filter {
                it.status == RectStatus.IsSelecting
            }.forEach {
                it.status = RectStatus.Selected
                it.color = newGeneratedColor
            }
            selectedItems.clear()
            shpManager?.increaseColorIndex(data!!.menuItemNumber)
            callbackInterface.onWordSelected(data!!.menuItemNumber, data!!.levelNumber,
                    wordNumber, RectStatus.Selected, newGeneratedColor)
        } else if (equalsEnum == LevelData.EqualsEnum.EQUALS_BUT_WRONG_POSITIONS) {
            callbackInterface.onWordSelectedWithWrongPosition()
        }
    }

    companion object {
        var shpManager: SharedPreferencesManager? = null

        val colorsArray = mutableListOf(
                CustomColor(255,145,132),
                CustomColor(201,214,123),
                CustomColor(76,189,190),
                CustomColor(96,142,183),
                CustomColor(35,173,255),
                CustomColor(22,167,82),
                CustomColor(151,170,241),
                CustomColor(192,141,191),
                CustomColor(229,109,188),
                CustomColor(198,143,102),
                CustomColor(221,136,133),
                CustomColor(122,158,156)
        )

        fun generateColor(menuNumber: Int): Int {
            val index = shpManager?.getCurrentColorIndex(menuNumber)
            val customColor = colorsArray[index!!]
            return Color.rgb(customColor.r, customColor.g, customColor.b)
        }
    }

    fun setSelectedWord(string: List<XYChar>) {
        string.forEach {
            selectedItems.add(RectItem(RectF(0f,0f,0f,0f), it.ch, 0, it.x, it.y))
        }
    }

    private fun getSelectedWord(): List<XYChar> {
        val builder = ArrayList<XYChar>()
        selectedItems.forEach {
            builder.add(XYChar(it.x, it.y, it.char!!))
        }
        return builder
    }

    private fun releaseAll() {
        rectItems.filter { it.status == RectStatus.IsSelecting }.forEach {
            it.clearItem()
        }

        currentSelectedItem = null
        selectedItems.clear()
        maxIndex = 1
    }

    private fun runOverData(data: LevelData, callback: RunOverCallback) {
        data.let {
            var x = 1
            var y = 1

            while (x <= data.chartSizeData.x) {
                while (y <= data.chartSizeData.y) {
                    callback.let { callback.getCoords(x, y) }
                    y++
                }
                x++
                y = 1
            }
        }
    }

    interface RunOverCallback {
        fun getCoords(x: Int, y: Int)
    }

    private val Int.px: Int
        get() = (this * Resources.getSystem().displayMetrics.density).toInt()

    fun getStatusByValue(status: Int): RectStatus {
        return when (status) {
            RectStatus.None.value -> RectStatus.None
            RectStatus.Selected.value -> RectStatus.Selected
            else -> RectStatus.IsSelecting
        }
    }

    fun spToPx(sp: Float): Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.resources.displayMetrics).toInt()
    }

    interface CallbackInterface {
        fun onWordSelected(menuItemNumber: Int, levelNumber: Int, wordNumber: Int, status: RectStatus, color: Int)
        fun onWordSelectedWithWrongPosition()
    }

    data class XYChar(val x: Int, val y: Int, val ch: Char)

    data class CustomColor(val r: Int, val g: Int, val b: Int)

}
