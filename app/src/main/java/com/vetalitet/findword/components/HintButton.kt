package com.vetalitet.findword.components

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.vetalitet.findword.R
import kotlinx.android.synthetic.main.hint_button_layout.view.*

class HintButton : LinearLayout {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init()
        initAttributes(context, attributeSet)
    }

    private fun initAttributes(context: Context, attributeSet: AttributeSet) {
        val array = context.obtainStyledAttributes(attributeSet, R.styleable.HintButton)
        val buttonText = array.getString(R.styleable.HintButton_hb_button_text)
        val coinsPrice = array.getString(R.styleable.HintButton_hb_coins_quantity)
        val coinsImage = array.getResourceId(R.styleable.HintButton_hb_coins_image, 0)
        val coinsMarginInside = array.getDimensionPixelSize(R.styleable.HintButton_hb_coins_margin_inside, 0)

        tvHintName.text = buttonText
        tvCoinsQuantity.text = coinsPrice
        iCoinsImage.setImageResource(coinsImage)

        val params = iCoinsImage.layoutParams as MarginLayoutParams
        params.leftMargin = coinsMarginInside
        params.rightMargin = coinsMarginInside
        params.topMargin = coinsMarginInside
        params.bottomMargin = coinsMarginInside
        iCoinsImage.layoutParams = params

        array.recycle()
    }

    private fun init() {
        inflate(context, R.layout.hint_button_layout, this)
    }

}