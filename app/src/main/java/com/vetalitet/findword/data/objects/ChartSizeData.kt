package com.vetalitet.findword.data.objects

data class ChartSizeData(val x: Int, val y: Int)