package com.vetalitet.findword.data.objects

data class WordData(val items: List<ItemData>) {

    fun isEqualTo(value: String): Boolean {
        val valueString = StringBuilder()
        items.forEach {
            valueString.append(it.char)
        }
        return valueString.toString().toUpperCase() == value.toUpperCase()
    }

}
