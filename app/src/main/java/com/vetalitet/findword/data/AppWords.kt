package com.vetalitet.findword.data

import java.util.*

object AppWords {

    val allWords: List<String>
        get() = Arrays.asList(
                "wolf",
                "alligator",
                "bison",
                "buffalo",
                "fox",
                "eagle",
                "bear"
        )

}
