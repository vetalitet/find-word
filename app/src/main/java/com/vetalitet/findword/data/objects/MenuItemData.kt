package com.vetalitet.findword.data.objects

data class MenuItemData(val menuNumber: Int, val name: String, val levels: ArrayList<LevelData>)