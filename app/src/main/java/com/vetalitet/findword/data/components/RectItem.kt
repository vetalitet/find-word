package com.vetalitet.findword.data.components

import android.graphics.RectF

class RectItem(val rect: RectF, val char: Char?, val defaultColor: Int, val x: Int, val y: Int) {

    var status: RectStatus = RectStatus.None
    var index: Int = 0
    var color: Int = defaultColor

    fun clearItem() {
        status = RectStatus.None
        index = 0
        color = defaultColor
    }

}
