package com.vetalitet.findword.data.algorythms

import com.vetalitet.findword.data.objects.ChartSizeData
import com.vetalitet.findword.data.objects.LevelData
import com.vetalitet.findword.data.objects.ItemData
import com.vetalitet.findword.data.objects.WordData
import java.util.ArrayList

class WordCreator(private val words: List<String>) {

    open fun generate3x3(x:Int, y:Int) : LevelData {
        val words = ArrayList<WordData>()

        val items1 = ArrayList<ItemData>()
        items1.add(ItemData(1, 3, 1, 1, 0, 0, 'b'))
        items1.add(ItemData(2, 3, 2, 1, 0, 0, 'i'))
        items1.add(ItemData(2, 2, 3, 1, 0, 0, 's'))
        items1.add(ItemData(1, 2, 4, 1, 0, 0, 'o'))
        items1.add(ItemData(1, 1, 5, 1, 0, 0, 'n'))
        words.add(WordData(items1))

        val items2 = ArrayList<ItemData>()
        items2.add(ItemData(3, 3, 6, 2, 0, 0, 'b'))
        items2.add(ItemData(3, 2, 7, 2, 0, 0, 'e'))
        items2.add(ItemData(3, 1, 8, 2, 0, 0, 'a'))
        items2.add(ItemData(2, 1, 9, 2, 0, 0, 'r'))
        words.add(WordData(items2))

        return LevelData(ChartSizeData(x, y), 1, 1, words)
    }

    open fun generate4x4(x:Int, y:Int) : LevelData {
        val words = ArrayList<WordData>()

        val items1 = ArrayList<ItemData>()
        items1.add(ItemData(4, 1, 10, 1, 0, 0, 'a'))
        items1.add(ItemData(3, 1, 11, 1, 0, 0, 'l'))
        items1.add(ItemData(3, 2, 12, 1, 0, 0, 'l'))
        items1.add(ItemData(4, 2, 13, 1, 0, 0, 'i'))
        items1.add(ItemData(4, 3, 14, 1, 0, 0, 'g'))
        items1.add(ItemData(3, 3, 15, 1, 0, 0, 'a'))
        items1.add(ItemData(2, 3, 16, 1, 0, 0, 't'))
        items1.add(ItemData(2, 2, 17, 1, 0, 0, 'o'))
        items1.add(ItemData(2, 1, 18, 1, 0, 0, 'r'))
        words.add(WordData(items1))

        val items2 = ArrayList<ItemData>()
        items2.add(ItemData(4, 4, 19, 2, 0, 0, 'f'))
        items2.add(ItemData(3, 4, 20, 2, 0, 0, 'o'))
        items2.add(ItemData(2, 4, 21, 2, 0, 0, 'x'))
        words.add(WordData(items2))

        val items3 = ArrayList<ItemData>()
        items3.add(ItemData(1, 4, 22, 3, 0, 0, 'w'))
        items3.add(ItemData(1, 3, 23, 3, 0, 0, 'o'))
        items3.add(ItemData(1, 2, 24, 3, 0, 0, 'l'))
        items3.add(ItemData(1, 1, 25, 3, 0, 0, 'f'))
        words.add(WordData(items3))

        return LevelData(ChartSizeData(x, y), 1, 2, words)
    }

}