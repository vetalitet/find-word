package com.vetalitet.findword.data.components

enum class RectStatus(open var value: Int) {
    None(0),
    Selected(1),
    IsSelecting(2);

}
