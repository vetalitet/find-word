package com.vetalitet.findword.data.objects

data class ItemData(val x: Int, val y: Int, val charNumber: Int, val wordNumber: Int, var status: Int, var color: Int, val char: Char)