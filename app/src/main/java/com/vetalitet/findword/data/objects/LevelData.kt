package com.vetalitet.findword.data.objects

import com.vetalitet.findword.components.WordTable
import com.vetalitet.findword.data.components.RectStatus

data class LevelData(val chartSizeData: ChartSizeData, val levelNumber: Int, val menuItemNumber: Int, val words: List<WordData>) {

    fun getCharBy(x: Int, y: Int) : Char? {
        for (word in words) {
            word.items
                    .filter { it.x == x && it.y == y }
                    .forEach { return it.char }
        }
        return null
    }

    fun getStatusBy(x: Int, y: Int) : Int {
        for (word in words) {
            word.items
                    .filter { it.x == x && it.y == y }
                    .forEach { return it.status }
        }
        return 0
    }

    fun getColorBy(x: Int, y: Int) : Int {
        for (word in words) {
            word.items
                    .filter { it.x == x && it.y == y }
                    .forEach { return it.color }
        }
        return 0
    }

    fun setSelectable(x: Int, y: Int) {
        for (word in words) {
            word.items
                    .filter { it.x == x && it.y == y }
                    .forEach { it.status = RectStatus.IsSelecting.value }
        }
    }

    fun clearSelectable() {
        for (word in words) {
            word.items
                    .filter { it.status == RectStatus.IsSelecting.value }
                    .forEach { it.status = RectStatus.None.value }
        }
    }

    fun setSelected(wordNumber: Int, color: Int) {
        for (word in words) {
            word.items
                    .filter { it.wordNumber == wordNumber }
                    .forEach {
                        it.status = RectStatus.Selected.value
                        it.color = color
                    }
        }
    }

    fun checkContainsWord(selectedWord: List<WordTable.XYChar>): Pair<Int, EqualsEnum> {
        words.forEach {
            val wordString = StringBuilder()
            val selectedString = StringBuilder()
            it.items.forEach {
                wordString.append(it.char)
            }
            selectedWord.forEach {
                selectedString.append(it.ch)
            }

            if (wordString.toString().toUpperCase() == selectedString.toString().toUpperCase()) {
                for (i in 0..it.items.size - 1) {
                    val item = it.items[i]
                    val value = selectedWord[i]
                    if (item.x != value.x || item.y != value.y) {
                        return Pair(0, EqualsEnum.EQUALS_BUT_WRONG_POSITIONS)
                    }
                }
                return Pair(it.items[0].wordNumber, EqualsEnum.EQUALS_COMPLETELY)
            }
        }

        return Pair(0, EqualsEnum.NOT_EQUALS)
    }

    fun getAllWithStatus(status: Int) : Int {
        var items = 0
        words.forEach {
            items = items.plus(it.items.filter { it.status == status }.size)
        }
        return items
    }

    enum class EqualsEnum {
        EQUALS_COMPLETELY, EQUALS_BUT_WRONG_POSITIONS, NOT_EQUALS
    }


}
