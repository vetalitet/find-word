package com.vetalitet.findword.di

interface HasComponent<out T> {
    val component: T
}