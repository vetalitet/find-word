package com.vetalitet.findword.di.module

import android.app.Activity
import com.vetalitet.findword.di.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    @PerActivity
    fun activity(): Activity = activity

}
