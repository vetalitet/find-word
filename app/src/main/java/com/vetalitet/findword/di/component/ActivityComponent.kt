package com.vetalitet.findword.di.component

import android.app.Activity
import com.vetalitet.findword.di.PerActivity
import com.vetalitet.findword.di.module.ActivityModule
import dagger.Component

@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun activity(): Activity
}