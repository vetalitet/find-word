package com.vetalitet.findword.di

import javax.inject.Scope

@Scope
@Retention
annotation class PerActivity
