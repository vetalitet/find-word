package com.vetalitet.findword.di.module

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemRepository
import com.vetalitet.findword.domain.domain.storage.database.DatabaseConfigurator
import com.vetalitet.findword.domain.domain.storage.preferences.Preferences
import com.vetalitet.findword.domain.domain.storage.preferences.PreferencesImpl
import com.vetalitet.findword.domain.executor.JobExecutor
import com.vetalitet.findword.domain.executor.MainThread
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.all.GetLevelItemsUseCase
import com.vetalitet.findword.domain.interactor.all.GetMenuItemsUseCase
import com.vetalitet.findword.managers.adv.AdvertisementManager
import com.vetalitet.findword.managers.analytics.AnalyticsManager
import com.vetalitet.findword.storage.LevelDiskDataStorage
import com.vetalitet.findword.storage.MenuItemDiskDataStorage
import com.vetalitet.findword.storage.realm.RealmDatabaseConfigurator
import dagger.Module
import dagger.Provides
import org.solovyev.android.checkout.Billing
import javax.inject.Singleton

@Module
class ApplicationModule(val context: Context) {

    @Provides
    fun provideApplicationContext() = context

    @Provides
    fun provideLayoutInflator(): LayoutInflater = LayoutInflater.from(context)

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(mainThread: MainThread): PostExecutionThread = mainThread

    @Singleton
    @Provides
    fun provideMenuItemRepository(menuItemDataStore: MenuItemDiskDataStorage): MenuItemRepository = MenuItemRepository(menuItemDataStore)

    @Singleton
    @Provides
    fun provideLevelRepository(levelDiskDataStorage: LevelDiskDataStorage): LevelRepository = LevelRepository(levelDiskDataStorage)

    @Provides
    @Singleton
    fun provideUserPreferences(preferencesImpl: PreferencesImpl): Preferences = preferencesImpl

    @Provides
    fun provideDatabaseConfigurator(databaseConfigurator: RealmDatabaseConfigurator): DatabaseConfigurator = databaseConfigurator

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences = context.getSharedPreferences(Preferences.preferencesName, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideAnalyticsManager(context: Context): AnalyticsManager = AnalyticsManager(context)

    @Provides
    @Singleton
    fun provideAdvertisementManager(context: Context, analyticsManager: AnalyticsManager): AdvertisementManager = AdvertisementManager(context, analyticsManager)

    @Provides
    @Singleton
    fun provideBilling(context: Context): Billing {
        return Billing(context, object : Billing.DefaultConfiguration() {
            override fun getPublicKey(): String {
                return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsf5kHhal7eNV0qwjfyMm8UuKd//XCTG" +
                        "1RKckvfQze08XozwtVwY3i/r6P+Mac67+FZey3D2nX8jMcqXF6NI1v5l/OW70xS235t414xd" +
                        "26EH9Y0sY7hILtHLG9JzLOSfVo+axtw2FBnFwk1vyH9qE2cjzsVonjjwq3WR99SQpwRhLSUv" +
                        "8/OgY9Avp4eqf9hzrmkoOh6gcqZm8HRmusFAq9eQ2XsNaJXVOBc5Bi8PAgrrqH63WYbzHNUl3" +
                        "0lVwCUaDuPNsNIpa18OL1tSyxJskYjaaKJ1zPRmo1Z0K29NDZvTZmuZbZpIowhyngx/kPwcJ47L" +
                        "4k0vLmlO10Eu/3lDPGQIDAQAB"
            }
        })
    }

}
