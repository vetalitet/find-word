package com.vetalitet.findword.di.component

import android.content.Context
import android.view.LayoutInflater
import com.vetalitet.findword.FindwordApplication
import com.vetalitet.findword.di.module.ApplicationModule
import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.screens.main.activity.MenuActivity
import com.vetalitet.findword.view.activity.BaseActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(application: FindwordApplication)

    fun inject(baseActivity: BaseActivity)

    fun inject(activity: MenuActivity)

    fun context(): Context

    fun layoutInflater(): LayoutInflater

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun provideMenuItemRepository(): MenuItemRepository

    fun provideLevelRepository(): LevelRepository

}
