package com.vetalitet.findword.view.activity

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.vetalitet.findword.FindwordApplication
import com.vetalitet.findword.di.module.ActivityModule
import com.vetalitet.findword.utils.helpers.executeTransaction

abstract class BaseActivity(@LayoutRes private val layoutResource: Int): AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResource)
        applicationComponent.inject(this)
    }

    protected val applicationComponent by lazy { (application as FindwordApplication).applicationComponent }

    protected val activityModule by lazy { ActivityModule(this) }

    protected fun replaceFragment(containerViewId: Int, fragment: Fragment, addToBackStack: Boolean = false) {
        supportFragmentManager.executeTransaction {
            if (addToBackStack) {
                addToBackStack(fragment.tag)
            }
            replace(containerViewId, fragment)
        }
    }

}
