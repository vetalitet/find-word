package com.vetalitet.findword.view.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.support.v4.content.LocalBroadcastManager
import android.util.Log

class NetworkStateChangeReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val networkStateIntent = Intent(NETWORK_AVAILABLE_ACTION)
        networkStateIntent.putExtra(IS_NETWORK_AVAILABLE, isConnectedToInternet(context))
        LocalBroadcastManager.getInstance(context).sendBroadcast(networkStateIntent)
    }

    private fun isConnectedToInternet(context: Context?): Boolean {
        try {
            if (context != null) {
                val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connectivityManager.activeNetworkInfo
                return networkInfo != null && networkInfo.isConnected
            }
            return false
        } catch (e: Exception) {
            Log.e(NetworkStateChangeReceiver::class.java.name, e.message)
            return false
        }

    }

    companion object {
        val NETWORK_AVAILABLE_ACTION = "com.vetalitet.crypto.NetworkAvailable"
        val IS_NETWORK_AVAILABLE = "isNetworkAvailable"
    }

}
