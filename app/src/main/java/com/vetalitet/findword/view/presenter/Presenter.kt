package com.vetalitet.findword.view.presenter

open class Presenter<V : Presenter.View> {

    protected var view: V? = null

    fun attachView(view: V) {
        this.view = view
    }

    fun detachView() {
        this.view = null
    }

    open fun resume() {

    }

    open fun pause() {

    }
    open fun destroy() {

    }

    interface View

}
