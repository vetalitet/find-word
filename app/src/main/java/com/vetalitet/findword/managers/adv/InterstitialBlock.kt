package com.vetalitet.findword.managers.adv

import android.content.Context
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.vetalitet.findword.managers.analytics.AnalyticsManager


class InterstitialBlock internal constructor(context: Context, analyticsManager: AnalyticsManager,
                                             categoty: String, val advParams: AdvParams) {

    private val interstitialAd: InterstitialAd?

    init {
        val isRealUser = advParams.isRealUser
        val realUserId = advParams.realInterstitialId
        val testUserId = advParams.testInterstitialId

        interstitialAd = InterstitialAd(context)
        interstitialAd.adUnitId = if (isRealUser) realUserId else testUserId
        interstitialAd.adListener = object : AdListener() {
            override fun onAdOpened() {
                super.onAdOpened()
                analyticsManager.logEvent(categoty + "Interstitial showed")
            }

            override fun onAdLoaded() {
                super.onAdLoaded()
                if (interstitialAd.isLoaded) {
                    interstitialAd.show()
                }
            }
        }
    }

    fun showInterstitial() {
        if (advParams.isAdvShown) {
            interstitialAd?.loadAd(AdRequest.Builder().build())
        }
    }

}
