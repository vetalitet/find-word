package com.vetalitet.findword.managers.adv

import android.content.Context
import com.google.android.gms.ads.AdView
import com.vetalitet.findword.managers.analytics.AnalyticsManager

class AdvertisementManager(private val context: Context, private val analyticsManager: AnalyticsManager) {

    private val advParams: AdvParams = AdvParams(context)

    fun createRewardedBlock() {

    }

    fun createInterstitialBlock(category: String): InterstitialBlock {
        return InterstitialBlock(context, analyticsManager, category, advParams)
    }

    fun createBannerBlock(advView: AdView, category: String): BannerBlock {
        return BannerBlock(analyticsManager, category, advParams, advView)
    }

}
