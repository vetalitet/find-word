package com.vetalitet.findword.managers.adv

class Constants {

    companion object {

        val mobAdRealId = "ca-app-pub-2204076909967153~4634010048"
        val mobAdTestId = "ca-app-pub-2204076909967153~6136711052"

        val xiaomiDeviceId = "4279C52867C2A7006C1C6D87DB7FD179"
        val lenovoDeviceId = "C1256277D1482C4ADA6E213F1DC44F52"
        val samsungS4DeviceId = "DA4349A4B02D88D015917F5D8E92DF"

        val adBannerRealId = "ca-app-pub-2204076909967153/4148888337"
        val adBannerTestId = "ca-app-pub-3940256099942544/1033173712"

        val adInterstitialRealId = "ca-app-pub-2204076909967153/4336165563"
        val adInterstitialTestId = "ca-app-pub-3940256099942544/1033173712"

        val adRevardedRealId = "ca-app-pub-2204076909967153/2340557610"
        val adRevardedTestId = "ca-app-pub-3940256099942544/5224354917"

    }

}