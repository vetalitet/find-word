package com.vetalitet.findword.managers.adv

import android.content.Context
import com.vetalitet.findword.R

class AdvParams(context: Context) {

    val realUserId: String = Constants.adBannerRealId
    val testUserId: String = Constants.adBannerTestId
    val realInterstitialId: String = Constants.adInterstitialRealId
    val testInterstitialId: String = Constants.adInterstitialTestId
    val xiaomiDeviceId: String = Constants.xiaomiDeviceId
    val lenovoDeviceId: String = Constants.lenovoDeviceId
    val samsungS4DeviceId: String = Constants.samsungS4DeviceId
    val isRealUser: Boolean = context.resources.getBoolean(R.bool.isAdvRealUser)
    val isAdvShown: Boolean = context.resources.getBoolean(R.bool.isAdvShown)

}
