package com.vetalitet.findword.managers.analytics

import android.content.Context
import android.util.Log
import com.google.android.gms.analytics.HitBuilders
import com.vetalitet.findword.FindwordApplication
import com.vetalitet.findword.R
import com.vetalitet.findword.utils.Utils
import java.util.*

class AnalyticsManager(private val context: Context) {

    private val isAnalyticsEnabled = context.resources.getBoolean(R.bool.isAnalyticsEnabled)

    fun logEvent(category: String, vararg action: String) {
        if (!isAnalyticsEnabled) return

        val enteredAction = if (action.isNotEmpty()) action[0] else null

        val eventBuilder = HitBuilders.EventBuilder()
        eventBuilder
                .setCategory(category)
                .setValue(1)
        if (enteredAction != null) {
            eventBuilder.setAction(enteredAction)
        }
        val data = eventBuilder.build()
        Log.d("AM", "event")
        (context as FindwordApplication).getDefaultTracker().send(data)
    }

    fun logEventWithTime(category: String) {
        if (!isAnalyticsEnabled) return

        logEvent(category, Utils.toTimeFormat(Date()))
    }

    fun logEventWithScreenName(category: String, screenName: String, vararg action: String) {
        if (!isAnalyticsEnabled) return

        (context as FindwordApplication).getDefaultTracker().setScreenName(screenName)
        logEvent(category, *action)
    }

}