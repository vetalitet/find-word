package com.vetalitet.findword.managers.adv

import android.content.Context
import android.view.View
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.vetalitet.findword.managers.analytics.AnalyticsManager


class BannerBlock(private val analyticsManager: AnalyticsManager,
                  private val category: String, advParams: AdvParams, view: AdView) {

    private val mAdView: AdView = view

    private val isRealUser: Boolean = advParams.isRealUser
    private val isAdvShown: Boolean = advParams.isAdvShown
    private val xiaomiDeviceId: String = advParams.xiaomiDeviceId
    private val lenovoDeviceId: String = advParams.lenovoDeviceId
    private val samsungS4DeviceId: String = advParams.samsungS4DeviceId
    private val realUserId: String = advParams.realUserId
    private val testUserId: String = advParams.testUserId

    fun showAdvert() {
        val advBuilder = AdRequest.Builder()
        if (!isRealUser) {
            advBuilder.addTestDevice(xiaomiDeviceId)
            advBuilder.addTestDevice(lenovoDeviceId)
            advBuilder.addTestDevice(samsungS4DeviceId)
        }
        val adRequest = advBuilder.build()
        mAdView.loadAd(adRequest)
        mAdView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                if (isAdvShown) {
                    mAdView.visibility = View.VISIBLE
                }
                analyticsManager.logEvent(category + " - showed")
            }

            override fun onAdOpened() {
                super.onAdOpened()
                analyticsManager.logEvent(category + " - clicked")
            }
        }
    }

    fun hideAdvert() {
        mAdView.visibility = View.GONE
    }

}
