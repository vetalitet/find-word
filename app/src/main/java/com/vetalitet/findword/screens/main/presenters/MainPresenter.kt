package com.vetalitet.findword.screens.main.presenters

import com.vetalitet.findword.domain.domain.menuItem.model.MenuItem
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItemStatus
import com.vetalitet.findword.domain.interactor.DefaultCompletableObserver
import com.vetalitet.findword.domain.interactor.DefaultSingleObserver
import com.vetalitet.findword.domain.interactor.all.GetMenuItemsUseCase
import com.vetalitet.findword.domain.interactor.all.UpdateCharStatusUseCase
import com.vetalitet.findword.domain.interactor.all.UpdateMenuItemStatusUseCase
import com.vetalitet.findword.screens.main.presenters.view.MenuView
import com.vetalitet.findword.view.presenter.Presenter
import io.reactivex.CompletableObserver
import javax.inject.Inject

class MainPresenter @Inject constructor(val getMenuItemsUseCase: GetMenuItemsUseCase,
                                        val updateCharStatusUseCase: UpdateMenuItemStatusUseCase) : Presenter<MenuView>() {

    fun getAllMenuItemsFromDB() {
        getMenuItemsUseCase.execute(GetMenuItemsUseCase.EmptyParams, object : DefaultSingleObserver<List<MenuItem>>() {
            override fun onSuccess(menuItems: List<MenuItem>) {
                view?.menuItemsLoaded(menuItems)
            }

            override fun onError(e: Throwable) {
                view?.onError(e.message)
            }
        })
    }

    fun enableMenuItem(menuItemForEnable: Int) {
        val params = UpdateMenuItemStatusUseCase.Params(menuItemForEnable, MenuItemStatus.ENABLED)
        updateCharStatusUseCase.execute(params, object : DefaultCompletableObserver(), CompletableObserver {
            override fun onComplete() {
                view?.menuItemEnabled()
            }

            override fun onError(e: Throwable) {
                view?.onError(e.message)
            }

        })
    }

}