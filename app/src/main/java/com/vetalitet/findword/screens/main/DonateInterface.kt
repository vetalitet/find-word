package com.vetalitet.findword.screens.main

interface DonateInterface {
    fun lowPriceButtonCall()
    fun highPriceButtonCall()
}
