package com.vetalitet.findword.screens.main.presenters.view

import com.vetalitet.findword.screens.main.OnError
import com.vetalitet.findword.view.presenter.Presenter

interface CheckCoinsAvailabilityView: Presenter.View, OnError {

    fun coinsEnough(hintType: Int)
    fun coinsNotEnough()

}