package com.vetalitet.findword.screens.main.presenters.view

import com.vetalitet.findword.domain.domain.menuItem.model.MenuItem
import com.vetalitet.findword.view.presenter.Presenter

interface MenuView : Presenter.View {

    fun menuItemsLoaded(menuItems: List<MenuItem>)
    fun menuItemEnabled()

    fun onError(message: String?)

}