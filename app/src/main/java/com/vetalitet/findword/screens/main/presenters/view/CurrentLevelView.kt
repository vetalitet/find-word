package com.vetalitet.findword.screens.main.presenters.view

import com.vetalitet.findword.screens.main.OnError
import com.vetalitet.findword.view.presenter.Presenter

interface CurrentLevelView: Presenter.View, OnError {

    fun levelIncreased()
    fun menuItemCompleted()

}
