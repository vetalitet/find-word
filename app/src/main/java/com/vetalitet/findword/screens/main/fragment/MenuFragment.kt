package com.vetalitet.findword.screens.main.fragment

import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.os.Handler
import android.support.annotation.DrawableRes
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.vetalitet.findword.CoinValues
import com.vetalitet.findword.R
import com.vetalitet.findword.components.FontChangeableTextView
import com.vetalitet.findword.components.FreeCoinsButton
import com.vetalitet.findword.components.SimpleDividerItemDecoration
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItem
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItemStatus
import com.vetalitet.findword.screens.main.MenuItemsAdapter
import com.vetalitet.findword.screens.main.di.MenuActivityComponent
import com.vetalitet.findword.screens.main.presenters.CheckAvailabilityPresenter
import com.vetalitet.findword.screens.main.presenters.GamePresenter
import com.vetalitet.findword.screens.main.presenters.MainPresenter
import com.vetalitet.findword.screens.main.presenters.view.CheckCoinsAvailabilityView
import com.vetalitet.findword.screens.main.presenters.view.GameView
import com.vetalitet.findword.screens.main.presenters.view.MenuView
import com.vetalitet.findword.view.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_menu_layout.*
import javax.inject.Inject
import android.view.WindowManager
import com.vetalitet.findword.components.BuyCoinsButton
import com.vetalitet.findword.managers.analytics.AnalyticsManager
import com.vetalitet.findword.screens.main.DonateInterface
import java.text.SimpleDateFormat
import java.util.*


class MenuFragment : BaseFragment(), MenuView, CheckCoinsAvailabilityView, GameView {

    @Inject
    internal lateinit var mainPresenter: MainPresenter
    @Inject
    internal lateinit var gamePresenter: GamePresenter
    @Inject
    internal lateinit var checkAvailabilityPresenter: CheckAvailabilityPresenter

    companion object {
        val ENABLE_LEVEL: Int = 1
    }

    var menuItemForEnable = 0
    var coins = 0

    internal lateinit var menuCallback: MenuCallback

    lateinit var dialog: AlertDialog

    //private val menuItemsAdapter by lazy { MenuItemsAdapter(activity, activity!!.layoutInflater) }
    lateinit var menuItemsAdapter: MenuItemsAdapter

    internal lateinit var analyticsManager: AnalyticsManager

    override val layoutResource: Int
        get() = R.layout.fragment_menu_layout

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        menuItemsAdapter = MenuItemsAdapter(activity, activity!!.layoutInflater)

        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = menuItemsAdapter
        recyclerView.addItemDecoration(SimpleDividerItemDecoration(context))

        menuItemsAdapter.onSelectMenuItem = { menuNumber: Int, categoryName: String ->
            menuCallback.goToGameFragment(menuNumber, categoryName)
        }
        menuItemsAdapter.onSelectedDisableItem = {
            menuItemForEnable = it
            showBuyLevelDialog(getString(R.string.oh_no),
                    getString(R.string.level_is_closed),
                    getString(R.string.open_level),
                    R.drawable.vector_info, {
                checkAvailabilityPresenter.checkCoinsAvailability(ENABLE_LEVEL, CoinValues.ENABLE_LEVEL_PAYMENT)
            })
        }
        menuItemsAdapter.onSelectedCompletedItem = {
            showCongratulationDialog(getString(R.string.brilliant_job),
                    getString(R.string.completed_category_message),
                    getString(R.string.choose_category),
                    R.drawable.ic_upvote, {})
        }

        getCoinsButton.setOnClickListener {
            getCoinsButton.isEnabled = false
            Handler().postDelayed({
                getCoinsButton.isEnabled = true
            }, 1000)
            showGetCoinsDialog(getString(R.string.get_coins))
        }
    }

    override fun onResume() {
        super.onResume()
        coinsCount.text = String.format(getString(R.string.coins_value), coins)
        gamePresenter.loadCoins()
    }

    private fun showGetCoinsDialog(title: String) {
        val view = layoutInflater.inflate(R.layout.dialog_get_coins_layout, null)

        val alert = AlertDialog.Builder(context)

        view.findViewById<FontChangeableTextView>(R.id.dialogTitle).text = title

        // this is set the view from XML inside AlertDialog
        alert.setView(view)
        // disallow cancel of AlertDialog on click of back button and outside touch
        //alert.setCancelable(false)

        view.findViewById<Button>(R.id.dialog_cancel).setOnClickListener {
            dialog.hide()
        }

        view.findViewById<FreeCoinsButton>(R.id.buttonFree).setOnClickListener {
            val sdf = SimpleDateFormat("hh:mm:ss", Locale.US)
            val currentDate = sdf.format(Date())
            analyticsManager.logEvent("Menu fragment", "free coins - $currentDate")
            menuCallback.showRewardedAdvertMethod()
        }

        view.findViewById<BuyCoinsButton>(R.id.lowPriceButton).setOnClickListener {
            menuCallback.lowPriceButtonCall()
        }

        view.findViewById<BuyCoinsButton>(R.id.highPriceButton).setOnClickListener {
            menuCallback.highPriceButtonCall()
        }

        dialog = alert.create()

        // Finally, display the alert dialog
        if (!dialog.isShowing) {
            dialog.show()
        }

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val point = Point()
        wm.defaultDisplay.getSize(point)
        val attributes = dialog.window.attributes
        attributes.width = (point.x * context.resources.getInteger(R.integer.dialog_percent)/ 100)
        dialog.window.attributes = attributes
    }

    private fun showCongratulationDialog(title: String, message: String, nextButtonText: String, @DrawableRes imageRes: Int, callback: () -> Unit ) {
        val view = layoutInflater.inflate(R.layout.dialog_congrat_layout, null)
        val alert = AlertDialog.Builder(context)

        view.findViewById<FontChangeableTextView>(R.id.title).text = title
        view.findViewById<FontChangeableTextView>(R.id.message).text = message
        view.findViewById<FontChangeableTextView>(R.id.nextButtonText).text = nextButtonText
        view.findViewById<ImageView>(R.id.image).setImageResource(imageRes)

        alert.setView(view)

        view.findViewById<LinearLayout>(R.id.nextLevelButton).setOnClickListener {
            callback()
            dialog.hide()
        }

        dialog = alert.create()

        if (!dialog.isShowing) {
            dialog.show()
        }

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val point = Point()
        wm.defaultDisplay.getSize(point)
        val attributes = dialog.window.attributes
        attributes.width = (point.x * context.resources.getInteger(R.integer.dialog_congrat_percent)/ 100)
        dialog.window.attributes = attributes
    }

    private fun showBuyLevelDialog(title: String, message: String, nextButtonText: String, @DrawableRes imageRes: Int, callback: () -> Unit) {
        val view = layoutInflater.inflate(R.layout.dialog_buy_disabled_level, null)
        val alert = AlertDialog.Builder(context)

        view.findViewById<FontChangeableTextView>(R.id.title).text = title
        view.findViewById<FontChangeableTextView>(R.id.message).text = message
        view.findViewById<FontChangeableTextView>(R.id.nextButtonText).text = nextButtonText
        view.findViewById<ImageView>(R.id.image).setImageResource(imageRes)

        alert.setView(view)
        //alert.setCancelable(false)

        view.findViewById<LinearLayout>(R.id.nextLevelButton).setOnClickListener {
            callback()
            dialog.hide()
        }

        view.findViewById<Button>(R.id.dialog_cancel).setOnClickListener {
            dialog.hide()
        }

        dialog = alert.create()
        if (!dialog.isShowing) {
            dialog.show()
        }

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val point = Point()
        wm.defaultDisplay.getSize(point)
        val attributes = dialog.window.attributes
        attributes.width = (point.x * context.resources.getInteger(R.integer.dialog_percent)/ 100)
        dialog.window.attributes = attributes
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeInjector()
        mainPresenter.attachView(this)
        mainPresenter.getAllMenuItemsFromDB()

        gamePresenter.attachView(this)
        analyticsManager = AnalyticsManager(activity.applicationContext)

        checkAvailabilityPresenter.attachView(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MenuCallback) {
            menuCallback = context
        }
    }

    override fun coinsEnough(hintType: Int) {
        if (hintType == ENABLE_LEVEL) {
            mainPresenter.enableMenuItem(menuItemForEnable)
        }
    }

    override fun coinsNotEnough() {
        if (dialog.isShowing) {
            dialog.hide()
        }

        showGetCoinsDialog("Not enough coins. Get more")
    }

    fun increaseRewardedCoins(amount: Int) {
        gamePresenter.increaseCoins(amount)
        if (dialog.isShowing) {
            dialog.hide()
        }
    }

    fun provideBoughtCoins(coinsQuantity: Int) {
        gamePresenter.increaseCoins(coinsQuantity)
    }

    override fun updateScore(score: Int) {

    }

    override fun updateCoins(coins: Int) {
        coinsCount.text = String.format(getString(R.string.coins_value), coins)
        this.coins = coins
    }

    private fun initializeInjector() {
        getComponent(MenuActivityComponent::class.java).inject(this)
    }

    override fun menuItemsLoaded(menuItems: List<MenuItem>) {
        val sortedMenuItems = menuItems.sortedBy { it.menuNumber }
        menuItemsAdapter.menuItems = sortedMenuItems
    }

    override fun menuItemEnabled() {
        gamePresenter.increaseCoins(-CoinValues.ENABLE_LEVEL_PAYMENT)
        menuItemsAdapter.menuItems.forEach {
            if (it.menuNumber == menuItemForEnable) {
                it.menuStatus = MenuItemStatus.ENABLED
            }
        }
        menuItemsAdapter.notifyDataSetChanged()
        if (dialog.isShowing) {
            dialog.hide()
        }
    }

    override fun onError(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    interface MenuCallback : DonateInterface {
        fun goToGameFragment(menuNumber: Int, categoryName: String)
        fun showRewardedAdvertMethod()
    }

}
