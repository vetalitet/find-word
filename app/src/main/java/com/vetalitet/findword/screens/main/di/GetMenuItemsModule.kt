package com.vetalitet.findword.screens.main.di

import com.vetalitet.findword.di.PerActivity
import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.all.GetMenuItemsUseCase
import dagger.Module
import dagger.Provides

@Module
class GetMenuItemsModule {

    @Provides
    @PerActivity
    fun provideGetMenuItemsUseCase(menuItemRepository: MenuItemRepository, threadExecutor: ThreadExecutor,
                                   postExecutionThread: PostExecutionThread): GetMenuItemsUseCase {
        return GetMenuItemsUseCase(menuItemRepository, threadExecutor, postExecutionThread)
    }

}