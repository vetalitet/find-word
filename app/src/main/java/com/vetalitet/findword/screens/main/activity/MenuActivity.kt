package com.vetalitet.findword.screens.main.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.vetalitet.findword.CoinValues
import com.vetalitet.findword.R
import com.vetalitet.findword.di.HasComponent
import com.vetalitet.findword.managers.adv.AdvertisementManager
import com.vetalitet.findword.managers.adv.Constants
import com.vetalitet.findword.managers.adv.InterstitialBlock
import com.vetalitet.findword.managers.analytics.AnalyticsManager
import com.vetalitet.findword.screens.main.di.DaggerMenuActivityComponent
import com.vetalitet.findword.screens.main.di.GetMenuItemsModule
import com.vetalitet.findword.screens.main.di.MenuActivityComponent
import com.vetalitet.findword.screens.main.fragment.MainFragment
import com.vetalitet.findword.screens.main.fragment.MenuFragment
import com.vetalitet.findword.view.activity.BaseActivity
import com.vetalitet.findword.view.receivers.NetworkStateChangeReceiver
import org.solovyev.android.checkout.*
import java.lang.Exception
import javax.inject.Inject


class MenuActivity : BaseActivity(R.layout.activity_main), HasComponent<MenuActivityComponent>,
        MenuFragment.MenuCallback, MainFragment.MainCallback, RewardedVideoAdListener {

    companion object {
        val MENU_NUMBER = "menu_number"
        val MENU_NAME = "menu_name"

        val LOW_PRICE_ITEM = "low_price"
        val HIGH_PRICE_ITEM = "high_price"
    }

    var REVARD_FRAGMENT = 0

    @BindView(R.id.adView)
    internal var adView: AdView? = null

    override val component by lazy { buildComponent() }

    lateinit var mRewardedVideoAd: RewardedVideoAd

    lateinit var interstitialBlock: InterstitialBlock

    @Inject
    internal lateinit var billing: Billing

    @Inject
    lateinit var advertisementManager: AdvertisementManager
    @Inject
    lateinit var analyticsManager: AnalyticsManager

    lateinit var checkout: ActivityCheckout

    private var lowPurchase: Purchase? = null
    private var highPurchase: Purchase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ButterKnife.bind(this)

        applicationComponent.inject(this)

        adView = findViewById(R.id.adView)

        //MobileAds.initialize(this, Constants.mobAdRealId)

        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this)
        mRewardedVideoAd.rewardedVideoAdListener = this

        loadAdvert()

        replaceFragment(R.id.content_frame, MenuFragment())

        loadAdvertisement()

        initCheckout()
    }

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter(NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION)
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
    }

    override fun onDestroy() {
        super.onDestroy()
        checkout.stop()
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val isNetworkAvailable = intent.getBooleanExtra(NetworkStateChangeReceiver.IS_NETWORK_AVAILABLE, false)
            if (isNetworkAvailable) {
                loadAdvertisement()
            }
        }
    }

    private fun loadAdvertisement() {
        val bannerBlock = advertisementManager.createBannerBlock(adView!!, "Banner (Main)")
        Handler().postDelayed({ bannerBlock.showAdvert() }, 250)

        interstitialBlock = advertisementManager.createInterstitialBlock("")
    }

    private fun loadAdvert() {
        val isRealUser = resources.getBoolean(R.bool.isAdvRealUser)
        mRewardedVideoAd.loadAd(if (isRealUser) Constants.adRevardedRealId else Constants.adRevardedTestId, AdRequest.Builder().build())
    }

    override fun goToGameFragment(menuNumber: Int, categoryName: String) {
        val fragment = MainFragment()
        val args = Bundle()
        args.putInt(MENU_NUMBER, menuNumber)
        args.putString(MENU_NAME, categoryName)
        fragment.arguments = args
        replaceFragment(R.id.content_frame, fragment, true)
    }

    override fun showRewardedAdvert() {
        REVARD_FRAGMENT = 1
        if (mRewardedVideoAd.isLoaded) {
            analyticsManager.logEvent("Main fragment", "show rewarded video")
            mRewardedVideoAd.show()
        }
    }

    override fun showRewardedAdvertMethod() {
        REVARD_FRAGMENT = 2
        if (mRewardedVideoAd.isLoaded) {
            analyticsManager.logEvent("Menu fragment", "show rewarded video")
            mRewardedVideoAd.show()
        }
    }

    override fun showInterstitialAdvert() {
        interstitialBlock.showInterstitial()
    }

    private fun buildComponent(): MenuActivityComponent {
        return DaggerMenuActivityComponent.builder()
                .applicationComponent(applicationComponent)
                .activityModule(activityModule)
                .getMenuItemsModule(GetMenuItemsModule())
                .build()
    }

    // ads

    override fun onRewardedVideoAdClosed() {
        loadAdvert()
    }

    override fun onRewardedVideoAdLeftApplication() {

    }

    override fun onRewardedVideoAdLoaded() {

    }

    override fun onRewardedVideoAdOpened() {

    }

    override fun onRewarded(rewardedItem: RewardItem) {
        if (REVARD_FRAGMENT == 1) {
            val fragment = supportFragmentManager.findFragmentById(R.id.content_frame) as MainFragment
            fragment.increaseRewardedCoins(rewardedItem.amount)
        } else if (REVARD_FRAGMENT == 2) {
            val fragment = supportFragmentManager.findFragmentById(R.id.content_frame) as MenuFragment
            fragment.increaseRewardedCoins(rewardedItem.amount)
        }
    }

    override fun onRewardedVideoStarted() {

    }

    override fun onRewardedVideoAdFailedToLoad(p0: Int) {

    }

    private fun initCheckout() {
        checkout = Checkout.forActivity(this, billing)
        checkout.start()
        checkout.loadInventory(Inventory.Request.create().loadAllPurchases(), object: Inventory.Callback {
            override fun onLoaded(products: Inventory.Products) {
                val product = products.get(ProductTypes.IN_APP)
                if (!product.supported) {
                    return
                }
                lowPurchase = product.getPurchaseInState(LOW_PRICE_ITEM, Purchase.State.PURCHASED)
                highPurchase = product.getPurchaseInState(HIGH_PRICE_ITEM, Purchase.State.PURCHASED)
            }
        })
    }

    override fun lowPriceButtonCall() {
        makePayment(lowPurchase, LOW_PRICE_ITEM)
    }

    override fun highPriceButtonCall() {
        makePayment(highPurchase, HIGH_PRICE_ITEM)
    }

    private fun makePayment(purchase: Purchase?, product: String) {
        if (purchase == null) {
            checkout.startPurchaseFlow(ProductTypes.IN_APP, product, null, object: EmptyRequestListener<Purchase>() {
                override fun onSuccess(currentPurchase: Purchase) {
                    //Toast.makeText(applicationContext, "purchase success: $product", Toast.LENGTH_LONG).show()
                    if (product == LOW_PRICE_ITEM) {
                        lowPurchase = currentPurchase
                    } else if (product == HIGH_PRICE_ITEM){
                        highPurchase = currentPurchase
                    }

                    onPurchasedChanged(currentPurchase)

                    checkout.whenReady(object: Checkout.EmptyListener() {
                        override fun onReady(requests: BillingRequests, p: String, billingSupported: Boolean) {
                            requests.consume(currentPurchase.token, object: RequestListener<Any> {
                                override fun onSuccess(result: Any) {
                                    //Toast.makeText(applicationContext, "consume success: $product", Toast.LENGTH_LONG).show()
                                    if (product == LOW_PRICE_ITEM) {
                                        lowPurchase = null
                                    } else if (product == HIGH_PRICE_ITEM){
                                        highPurchase = null
                                    }
                                }

                                override fun onError(response: Int, e: Exception) {
                                    //Toast.makeText(applicationContext, "consume error", Toast.LENGTH_LONG).show()
                                }
                            })
                        }
                    })
                }

                override fun onError(response: Int, e: Exception) {
                    super.onError(response, e)
                    Toast.makeText(applicationContext, "purchase error", Toast.LENGTH_LONG).show()
                }
            })
        }
    }

    private fun onPurchasedChanged(purchase: Purchase?) {
        val coinsQuantity = when {
            purchase?.sku == LOW_PRICE_ITEM -> CoinValues.LOW_PAID_REWARD
            purchase?.sku == HIGH_PRICE_ITEM -> CoinValues.HIGH_PAID_REWARD
            else -> 0
        }
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        when (fragment) {
            is MenuFragment -> fragment.provideBoughtCoins(coinsQuantity)
            is MainFragment -> fragment.provideBoughtCoins(coinsQuantity)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        checkout.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

}
