package com.vetalitet.findword.screens.main.presenters

import com.vetalitet.findword.domain.interactor.DefaultCompletableObserver
import com.vetalitet.findword.domain.interactor.all.FinishMenuItemUseCase
import com.vetalitet.findword.domain.interactor.all.IncreaseLevelUseCase
import com.vetalitet.findword.screens.main.presenters.view.CurrentLevelView
import com.vetalitet.findword.view.presenter.Presenter
import javax.inject.Inject

class IncreaseCurrentLevelPresenter @Inject constructor(
        private val increaseLevelUseCase: IncreaseLevelUseCase,
        private val finishMenuItemUseCase: FinishMenuItemUseCase) : Presenter<CurrentLevelView>() {

    fun increaseLevel(menuNumber: Int) {
        val params = IncreaseLevelUseCase.Params(menuNumber)
        increaseLevelUseCase.execute(params, object : DefaultCompletableObserver() {
            override fun onComplete() {
                super.onComplete()
                view?.levelIncreased()
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

    fun finishMenuNumber(menuNumber: Int) {
        val params = FinishMenuItemUseCase.Params(menuNumber)
        finishMenuItemUseCase.execute(params, object : DefaultCompletableObserver() {
            override fun onComplete() {
                super.onComplete()
                view?.menuItemCompleted()
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

}