package com.vetalitet.findword.screens.main

interface OnError {

    fun onError(message: String?)

}