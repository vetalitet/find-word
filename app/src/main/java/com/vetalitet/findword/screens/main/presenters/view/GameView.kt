package com.vetalitet.findword.screens.main.presenters.view

import com.vetalitet.findword.screens.main.OnError
import com.vetalitet.findword.view.presenter.Presenter

interface GameView: Presenter.View, OnError {

    fun updateScore(score: Int)
    fun updateCoins(coins: Int)

}