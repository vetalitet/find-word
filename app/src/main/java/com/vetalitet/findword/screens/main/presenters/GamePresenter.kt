package com.vetalitet.findword.screens.main.presenters

import com.vetalitet.findword.domain.interactor.DefaultSingleObserver
import com.vetalitet.findword.domain.interactor.all.GetCoinsUseCase
import com.vetalitet.findword.domain.interactor.all.GetScoreUseCase
import com.vetalitet.findword.domain.interactor.all.IncreaseCoinsUseCase
import com.vetalitet.findword.domain.interactor.all.IncreaseScoreUseCase
import com.vetalitet.findword.screens.main.presenters.view.GameView
import com.vetalitet.findword.view.presenter.Presenter
import javax.inject.Inject

class GamePresenter @Inject constructor(private val increaseScoreUseCase: IncreaseScoreUseCase,
                                        private val increaseCoinsUseCase: IncreaseCoinsUseCase,
                                        private val getScoreUseCase: GetScoreUseCase,
                                        private val getCoinsUseCase: GetCoinsUseCase) : Presenter<GameView>() {

    fun increaseScore() {
        val params = IncreaseScoreUseCase.Params(1)
        increaseScoreUseCase.execute(params, object : DefaultSingleObserver<Int>() {
            override fun onSuccess(score: Int) {
                super.onSuccess(score)
                view?.updateScore(score)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

    fun increaseCoins(value: Int) {
        val params = IncreaseCoinsUseCase.Params(value)
        increaseCoinsUseCase.execute(params, object : DefaultSingleObserver<Int>() {
            override fun onSuccess(coins: Int) {
                super.onSuccess(coins)
                view?.updateCoins(coins)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

    fun loadScore() {
        getScoreUseCase.execute(GetScoreUseCase.Empty, object : DefaultSingleObserver<Int>() {
            override fun onSuccess(score: Int) {
                super.onSuccess(score)
                view?.updateScore(score)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

    fun loadCoins() {
        getCoinsUseCase.execute(GetCoinsUseCase.Empty, object : DefaultSingleObserver<Int>() {
            override fun onSuccess(t: Int) {
                super.onSuccess(t)
                view?.updateCoins(t)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

}
