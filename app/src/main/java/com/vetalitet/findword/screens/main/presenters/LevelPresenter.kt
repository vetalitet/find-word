package com.vetalitet.findword.screens.main.presenters

import com.vetalitet.findword.data.components.RectStatus
import com.vetalitet.findword.data.objects.LevelData
import com.vetalitet.findword.domain.interactor.DefaultCompletableObserver
import com.vetalitet.findword.domain.interactor.DefaultSingleObserver
import com.vetalitet.findword.domain.interactor.all.*
import com.vetalitet.findword.screens.main.presenters.view.LevelView
import com.vetalitet.findword.view.presenter.Presenter
import javax.inject.Inject

class LevelPresenter @Inject constructor(private val getLevelItemsUseCase: GetLevelItemsUseCase,
                                         private val updateCharStatusUseCase: UpdateCharStatusUseCase,
                                         private val checkAllCharsSelectedUseCase: CheckAllCharsSelectedUseCase,
                                         private val findHintUseCase: FindHintUseCase,
                                         private val findWordUseCase: FindWordUseCase) : Presenter<LevelView>() {

    fun getLevel(menuNumber: Int) {
        val params = GetLevelItemsUseCase.Params(menuNumber)
        getLevelItemsUseCase.execute(params, object : DefaultSingleObserver<LevelData>() {
            override fun onSuccess(t: LevelData) {
                view?.levelLoaded(t)
            }

            override fun onError(e: Throwable) {
                view?.onError(e.message)
            }
        })
    }

    fun updateCharStatus(menuItemNumber: Int, levelNumber: Int, wordNumber: Int, status: RectStatus, color: Int) {
        val params = UpdateCharStatusUseCase.Params(menuItemNumber, levelNumber, wordNumber, status, color)
        updateCharStatusUseCase.execute(params, object : DefaultCompletableObserver() {
            override fun onComplete() {
                super.onComplete()
                view?.wordSelected(menuItemNumber, levelNumber, wordNumber, color)
            }

            override fun onError(e: Throwable) {
                view?.onError(e.message)
            }
        })
    }

    fun checkAllCharsSelected(menuItemNumber: Int, levelNumber: Int) {
        val params = CheckAllCharsSelectedUseCase.Params(menuItemNumber, levelNumber)
        checkAllCharsSelectedUseCase.execute(params, object : DefaultSingleObserver<Boolean>() {
            override fun onSuccess(t: Boolean) {
                super.onSuccess(t)
                if (t) {
                    view?.levelCompleted(levelNumber)
                }
             }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

    fun findLetterHint(menuNumber: Int) {
        val params = FindHintUseCase.Params(menuNumber)
        findHintUseCase.execute(params, object : DefaultSingleObserver<Pair<Int, Int>>() {
            override fun onSuccess(t: Pair<Int, Int>) {
                super.onSuccess(t)
                view?.hintPosition(t.first, t.second)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

    fun findWordHint(menuNumber: Int) {
        val params = FindWordUseCase.Params(menuNumber)
        findWordUseCase.execute(params, object : DefaultSingleObserver<FindWordUseCase.ReturnObject>() {
            override fun onSuccess(t: FindWordUseCase.ReturnObject) {
                super.onSuccess(t)
                view?.hintPosition(t)
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

}
