package com.vetalitet.findword.screens.main.presenters.view

import com.vetalitet.findword.data.objects.LevelData
import com.vetalitet.findword.domain.interactor.all.FindWordUseCase
import com.vetalitet.findword.screens.main.OnError
import com.vetalitet.findword.view.presenter.Presenter

interface LevelView : Presenter.View, OnError {

    fun levelLoaded(levelData: LevelData)

    fun levelCompleted(levelNumber: Int)
    fun wordSelected(menuItemNumber: Int, levelNumber: Int, wordNumber: Int, color: Int)
    fun hintPosition(x: Int, y: Int)
    fun hintPosition(x: FindWordUseCase.ReturnObject)

}
