package com.vetalitet.findword.screens.main.di

import com.vetalitet.findword.di.PerActivity
import com.vetalitet.findword.domain.domain.menuItem.repository.level.LevelRepository
import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.all.*
import dagger.Module
import dagger.Provides

@Module
class LevelModule {

    @Provides
    @PerActivity
    fun provideGetLevelItemsUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                                    postExecutionThread: PostExecutionThread): GetLevelItemsUseCase {
        return GetLevelItemsUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideUpdateCharStatusUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                                       postExecutionThread: PostExecutionThread): UpdateCharStatusUseCase {
        return UpdateCharStatusUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideUpdateMenuItemStatusUseCase(menuItemRepository: MenuItemRepository, threadExecutor: ThreadExecutor,
                                       postExecutionThread: PostExecutionThread): UpdateMenuItemStatusUseCase {
        return UpdateMenuItemStatusUseCase(menuItemRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideIncreaseScoreUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                                    postExecutionThread: PostExecutionThread): IncreaseScoreUseCase {
        return IncreaseScoreUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideIncreaseCoinsUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                                    postExecutionThread: PostExecutionThread): IncreaseCoinsUseCase {
        return IncreaseCoinsUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideGetScoreUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                               postExecutionThread: PostExecutionThread): GetScoreUseCase {
        return GetScoreUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideGetCoinsUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                               postExecutionThread: PostExecutionThread): GetCoinsUseCase {
        return GetCoinsUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideIncreaseLevelUseCase(menuItemRepository: MenuItemRepository, threadExecutor: ThreadExecutor,
                                    postExecutionThread: PostExecutionThread): IncreaseLevelUseCase {
        return IncreaseLevelUseCase(menuItemRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideCheckAllCharsSelectedUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                                    postExecutionThread: PostExecutionThread): CheckAllCharsSelectedUseCase {
        return CheckAllCharsSelectedUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideFinishMenuItemUseCase(menuItemRepository: MenuItemRepository, threadExecutor: ThreadExecutor,
                                            postExecutionThread: PostExecutionThread): FinishMenuItemUseCase {
        return FinishMenuItemUseCase(menuItemRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideFindHintUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                                     postExecutionThread: PostExecutionThread): FindHintUseCase {
        return FindHintUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideFindWordUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                               postExecutionThread: PostExecutionThread): FindWordUseCase {
        return FindWordUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

    @Provides
    @PerActivity
    fun provideCheckCoinsAvailabilityUseCase(levelRepository: LevelRepository, threadExecutor: ThreadExecutor,
                               postExecutionThread: PostExecutionThread): CheckCoinsAvailabilityUseCase {
        return CheckCoinsAvailabilityUseCase(levelRepository, threadExecutor, postExecutionThread)
    }

}
