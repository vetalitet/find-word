package com.vetalitet.findword.screens.splash.di

import com.vetalitet.findword.di.PerActivity
import com.vetalitet.findword.di.component.ActivityComponent
import com.vetalitet.findword.di.component.ApplicationComponent
import com.vetalitet.findword.di.module.ActivityModule
import com.vetalitet.findword.screens.splash.fragment.SplashFragment
import dagger.Component

@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(ActivityModule::class, CopyFileDataToDBModule::class))
interface SplashActivityComponent : ActivityComponent {

    fun inject(fragment: SplashFragment)

}
