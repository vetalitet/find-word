package com.vetalitet.findword.screens.main

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.vetalitet.findword.R
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItem
import com.vetalitet.findword.domain.domain.menuItem.model.MenuItemStatus
import kotlinx.android.synthetic.main.item_menu.view.*
import kotlin.properties.Delegates

class MenuItemsAdapter(private val context: Context,
                       private val layoutInflater: LayoutInflater) : RecyclerView.Adapter<MenuItemViewHolder>() {

    var onSelectMenuItem: ((Int, String) -> Unit) = { menuNumber: Int, categoryName: String -> }
    var onSelectedDisableItem: ((Int) -> Unit) = {}
    var onSelectedCompletedItem: ((Int) -> Unit) = {}

    var menuItems by Delegates.observable(emptyList<MenuItem>()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MenuItemViewHolder {
        val view = layoutInflater.inflate(R.layout.item_menu, parent, false)
        return MenuItemViewHolder(view, { onItemClick(it) })
    }

    private fun onItemClick(position: Int) {
        val menuItem = menuItems[position]
        if (menuItem.menuStatus == MenuItemStatus.ENABLED) {
            onSelectMenuItem.invoke(menuItem.menuNumber, menuItem.menuName)
        }
        if (menuItem.menuStatus == MenuItemStatus.DISABLED) {
            onSelectedDisableItem.invoke(menuItem.menuNumber)
        }
        if (menuItem.menuStatus == MenuItemStatus.COMPLETED) {
            onSelectedCompletedItem.invoke(menuItem.menuNumber)
        }
    }

    override fun onBindViewHolder(viewHolder: MenuItemViewHolder, position: Int) {
        val menuItem = menuItems[position]
        viewHolder.bind(menuItem, context)
    }

    override fun getItemCount() = menuItems.size

}

class MenuItemViewHolder(itemView: View, onItemClick: (position: Int) -> Unit) : RecyclerView.ViewHolder(itemView) {

    private val itemName = itemView.findViewById<TextView>(R.id.itemName)
    private val menuItemImage = itemView.findViewById<ImageView>(R.id.menuItemImage)

    init {
        itemView.setOnClickListener { onItemClick(layoutPosition) }
    }

    fun bind(menuItem: MenuItem, context: Context) {
        itemName.text = menuItem.menuName
        val menuNumber = menuItem.menuNumber

        when (menuItem.menuStatus) {
            MenuItemStatus.ENABLED -> {
                itemView.setBackgroundResource(R.drawable.selector_menu_item)
                itemView.itemName.setTextColor(context.resources.getColor(R.color.menu_item_16324c))
                itemView.available.visibility = View.VISIBLE
                itemView.imgChecked.visibility = View.GONE
                itemView.lockedImageHolder.visibility = View.GONE
                itemView.disabledArea.visibility = View.GONE
                itemView.leftMark.visibility = View.VISIBLE
                itemView.leftMark.setBackgroundColor(context.resources.getColor(R.color.button_light_purple))
                menuItemImage.setImageDrawable(getDrawable( "i$menuNumber", context))
            }
            MenuItemStatus.COMPLETED -> {
                itemView.setBackgroundResource(R.drawable.color_menu_item_completed)
                itemView.imgChecked.visibility = View.VISIBLE
                itemView.available.visibility = View.GONE
                itemView.lockedImageHolder.visibility = View.GONE
                itemView.disabledArea.visibility = View.GONE
                itemView.itemName.setTextColor(context.resources.getColor(R.color.button_blue))
                itemView.leftMark.visibility = View.VISIBLE
                itemView.leftMark.setBackgroundColor(context.resources.getColor(R.color.button_blue))
                menuItemImage.setImageDrawable(getDrawable( "i$menuNumber"+"c", context))
            }
            else -> {
                itemView.setBackgroundResource(R.drawable.color_menu_item_disabled)
                itemView.disabledArea.visibility = View.VISIBLE
                itemView.itemName.setTextColor(context.resources.getColor(R.color.dark_gray_blue))
                itemView.lockedImageHolder.visibility = View.VISIBLE
                itemView.leftMark.visibility = View.INVISIBLE
                menuItemImage.setImageDrawable(getDrawable( "i$menuNumber", context))
            }
        }
    }

    fun getDrawable(name: String, context: Context): Drawable {
        val resources = context.resources
        val identifier = resources.getIdentifier(name, "drawable", context.packageName)
        return resources.getDrawable(identifier)
    }

}
