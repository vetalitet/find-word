package com.vetalitet.findword.screens.splash.activity

import android.content.Intent
import android.os.Bundle
import com.vetalitet.findword.R
import com.vetalitet.findword.di.HasComponent
import com.vetalitet.findword.screens.main.activity.MenuActivity
import com.vetalitet.findword.screens.splash.di.SplashActivityComponent
import com.vetalitet.findword.screens.splash.di.CopyFileDataToDBModule
import com.vetalitet.findword.screens.splash.di.DaggerSplashActivityComponent
import com.vetalitet.findword.screens.splash.fragment.SplashFragment
import com.vetalitet.findword.view.activity.BaseActivity

class SplashActivity: BaseActivity(R.layout.activity_splash), SplashFragment.SplashCallback,
        HasComponent<SplashActivityComponent> {

    override val component by lazy { buildComponent() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            showSplashFragment()
        }
    }

    private fun buildComponent(): SplashActivityComponent {
        return DaggerSplashActivityComponent.builder()
                .applicationComponent(applicationComponent)
                .activityModule(activityModule)
                .copyFileDataToDBModule(CopyFileDataToDBModule())
                .build()
    }

    private fun showSplashFragment() {
        val splashFragment = SplashFragment()
        replaceFragment(R.id.content_frame, splashFragment)
    }

    override fun goToMainFragment() {
        finish()

        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
    }

}
