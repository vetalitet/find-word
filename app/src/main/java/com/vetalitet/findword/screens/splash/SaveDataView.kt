package com.vetalitet.findword.screens.splash

import com.vetalitet.findword.view.presenter.Presenter

interface SaveDataView: Presenter.View {

    fun dataSaved()
    fun onError(message: String?)

}