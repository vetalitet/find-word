package com.vetalitet.findword.screens.splash.di

import android.content.Context
import com.vetalitet.findword.di.PerActivity
import com.vetalitet.findword.domain.domain.menuItem.repository.menu.MenuItemRepository
import com.vetalitet.findword.domain.executor.PostExecutionThread
import com.vetalitet.findword.domain.executor.ThreadExecutor
import com.vetalitet.findword.domain.interactor.database.CopyFileDataToDBUseCase
import dagger.Module
import dagger.Provides

@Module
class CopyFileDataToDBModule {

    @Provides
    @PerActivity
    fun provideCopyFileDataToDBUseCase(context: Context, menuItemRepository: MenuItemRepository,
                                       threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread): CopyFileDataToDBUseCase {
        return CopyFileDataToDBUseCase(context, menuItemRepository, threadExecutor, postExecutionThread)
    }

}
