package com.vetalitet.findword.screens.main.di

import com.vetalitet.findword.di.PerActivity
import com.vetalitet.findword.di.component.ActivityComponent
import com.vetalitet.findword.di.component.ApplicationComponent
import com.vetalitet.findword.di.module.ActivityModule
import com.vetalitet.findword.screens.main.fragment.MainFragment
import com.vetalitet.findword.screens.main.fragment.MenuFragment
import dagger.Component

@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(ActivityModule::class, GetMenuItemsModule::class, LevelModule::class))
interface MenuActivityComponent : ActivityComponent {

    fun inject(fragment: MainFragment)
    fun inject(fragment: MenuFragment)

}