package com.vetalitet.findword.screens.splash

import com.vetalitet.findword.domain.interactor.DefaultCompletableObserver
import com.vetalitet.findword.domain.interactor.database.CopyFileDataToDBUseCase
import com.vetalitet.findword.view.presenter.Presenter
import javax.inject.Inject

class SplashPresenter @Inject constructor(val copyFileDataToDBUseCase: CopyFileDataToDBUseCase) : Presenter<SaveDataView>() {

    fun parseDataFromFileAndStoreToDB() {
        copyFileDataToDBUseCase.execute(CopyFileDataToDBUseCase.EmptyParams, object : DefaultCompletableObserver() {
            override fun onComplete() {
                view?.dataSaved()
            }

            override fun onError(e: Throwable) {
                view?.onError(e.message)
            }
        })
    }

}
