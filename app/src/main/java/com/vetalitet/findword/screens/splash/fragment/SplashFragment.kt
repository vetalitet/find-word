package com.vetalitet.findword.screens.splash.fragment

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.vetalitet.findword.R
import com.vetalitet.findword.screens.splash.SaveDataView
import com.vetalitet.findword.screens.splash.SplashPresenter
import com.vetalitet.findword.screens.splash.di.SplashActivityComponent
import com.vetalitet.findword.view.fragment.BaseFragment
import javax.inject.Inject

class SplashFragment: BaseFragment(), SaveDataView {

    @Inject
    internal lateinit var presenter: SplashPresenter

    internal lateinit var splashCallback: SplashCallback

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeInjector()

        presenter.attachView(this)
        presenter.parseDataFromFileAndStoreToDB()
    }

    override val layoutResource: Int
        get() = R.layout.fragment_splash_layout

    private fun initializeInjector() {
        getComponent(SplashActivityComponent::class.java).inject(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is SplashCallback) {
            splashCallback = context
        }
    }

    override fun dataSaved() {
        Handler().postDelayed({
            splashCallback.goToMainFragment()
        }, 1500)
    }

    override fun onError(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    interface SplashCallback {
        fun goToMainFragment()
    }

}
