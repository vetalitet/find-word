package com.vetalitet.findword.screens.main.fragment

import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.os.Handler
import android.support.annotation.DrawableRes
import android.support.v7.app.AlertDialog
import android.view.Gravity
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.vetalitet.findword.CoinValues
import com.vetalitet.findword.R
import com.vetalitet.findword.components.BuyCoinsButton
import com.vetalitet.findword.components.FontChangeableTextView
import com.vetalitet.findword.components.FreeCoinsButton
import com.vetalitet.findword.components.WordTable
import com.vetalitet.findword.data.components.RectStatus
import com.vetalitet.findword.data.objects.LevelData
import com.vetalitet.findword.domain.interactor.all.FindWordUseCase
import com.vetalitet.findword.managers.analytics.AnalyticsManager
import com.vetalitet.findword.screens.main.DonateInterface
import com.vetalitet.findword.screens.main.activity.MenuActivity
import com.vetalitet.findword.screens.main.di.MenuActivityComponent
import com.vetalitet.findword.screens.main.presenters.CheckAvailabilityPresenter
import com.vetalitet.findword.screens.main.presenters.GamePresenter
import com.vetalitet.findword.screens.main.presenters.IncreaseCurrentLevelPresenter
import com.vetalitet.findword.screens.main.presenters.LevelPresenter
import com.vetalitet.findword.screens.main.presenters.view.CheckCoinsAvailabilityView
import com.vetalitet.findword.screens.main.presenters.view.CurrentLevelView
import com.vetalitet.findword.screens.main.presenters.view.GameView
import com.vetalitet.findword.screens.main.presenters.view.LevelView
import com.vetalitet.findword.view.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_main_layout.*
import kotlinx.android.synthetic.main.toast_hint_word.view.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class MainFragment : BaseFragment(), LevelView, GameView, CurrentLevelView, CheckCoinsAvailabilityView {

    @Inject
    internal lateinit var presenter: LevelPresenter

    @Inject
    internal lateinit var gamePresenter: GamePresenter

    @Inject
    internal lateinit var checkAvailabilityPresenter: CheckAvailabilityPresenter

    @Inject
    internal lateinit var levelIncreaserPresenter: IncreaseCurrentLevelPresenter

    internal lateinit var mainCallback: MainFragment.MainCallback

    lateinit var analyticsManager: AnalyticsManager

    var menuNumber = 0
    var menuName = ""

    lateinit var dialog: AlertDialog

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initializeInjector()

        menuNumber = arguments.getInt(MenuActivity.MENU_NUMBER, 1)
        menuName = arguments.getString(MenuActivity.MENU_NAME, "")

        category.text = menuName

        worldTable.callbackInterface = object : WordTable.CallbackInterface {
            override fun onWordSelectedWithWrongPosition() {
                showCongratulationDialog("Hmm!", "This word exists. But try to make it up in another way",
                        "Ok, let's try", R.drawable.ic_klapovukhyi, {
                })
            }

            override fun onWordSelected(menuItemNumber: Int, levelNumber: Int, wordNumber: Int, status: RectStatus, color: Int) {
                presenter.updateCharStatus(menuItemNumber, levelNumber, wordNumber, status, color)
                gamePresenter.increaseScore()
            }
        }

        backLayout.setOnClickListener {
            fragmentManager.popBackStack()
        }

        hintLetterButton.setOnClickListener({
            hintLetterButton.isEnabled = false
            hintWordButton.isEnabled = false
            getCoinsButton.isEnabled = false
            Handler().postDelayed({
                hintLetterButton.isEnabled = true
                hintWordButton.isEnabled = true
                getCoinsButton.isEnabled = true
            }, 1000)
            worldTable.getData()?.clearSelectable()
            checkAvailabilityPresenter.checkCoinsAvailability(1, CoinValues.LETTER_HINT_PAYMENT)
        })

        hintWordButton.setOnClickListener {
            hintLetterButton.isEnabled = false
            hintWordButton.isEnabled = false
            getCoinsButton.isEnabled = false
            Handler().postDelayed({
                hintLetterButton.isEnabled = true
                hintWordButton.isEnabled = true
                getCoinsButton.isEnabled = true
            }, 1000)
            worldTable.getData()?.clearSelectable()
            checkAvailabilityPresenter.checkCoinsAvailability(2, CoinValues.WORD_HINT_PAYMENT)
        }

        getCoinsButton.setOnClickListener {
            getCoinsButton.isEnabled = false
            hintLetterButton.isEnabled = false
            hintWordButton.isEnabled = false
            Handler().postDelayed({
                getCoinsButton.isEnabled = true
                hintLetterButton.isEnabled = true
                hintWordButton.isEnabled = true
            }, 1000)
            showGetCoinsDialog(getString(R.string.get_coins))
        }

        analyticsManager = AnalyticsManager(activity.applicationContext)

        presenter.attachView(this)
        presenter.getLevel(menuNumber)

        gamePresenter.attachView(this)
        gamePresenter.loadScore()
        gamePresenter.loadCoins()

        levelIncreaserPresenter.attachView(this)
        checkAvailabilityPresenter.attachView(this)

        val layoutParams = hintButtons.layoutParams
        layoutParams.width = worldTable.layoutParams.width
        hintButtons.layoutParams = layoutParams
    }

    private fun showGetCoinsDialog(title: String) {
        val view = layoutInflater.inflate(R.layout.dialog_get_coins_layout, null)

        val alert = AlertDialog.Builder(context)

        view.findViewById<FontChangeableTextView>(R.id.dialogTitle).text = title

        // this is set the view from XML inside AlertDialog
        alert.setView(view)
        // disallow cancel of AlertDialog on click of back button and outside touch
        //alert.setCancelable(false)

        view.findViewById<Button>(R.id.dialog_cancel).setOnClickListener {
            dialog.hide()
        }

        view.findViewById<FreeCoinsButton>(R.id.buttonFree).setOnClickListener {
            val sdf = SimpleDateFormat("hh:mm:ss", Locale.US)
            val currentDate = sdf.format(Date())
            analyticsManager.logEvent("Main fragment", "free coins - $currentDate")
            mainCallback.showRewardedAdvert()
        }

        view.findViewById<BuyCoinsButton>(R.id.lowPriceButton).setOnClickListener {
            mainCallback.lowPriceButtonCall()
        }

        view.findViewById<BuyCoinsButton>(R.id.highPriceButton).setOnClickListener {
            mainCallback.highPriceButtonCall()
        }

        dialog = alert.create()

        // Finally, display the alert dialog
        if (!dialog.isShowing) {
            dialog.show()
        }

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val point = Point()
        wm.defaultDisplay.getSize(point)
        val attributes = dialog.window.attributes
        attributes.width = (point.x * context.resources.getInteger(R.integer.dialog_percent) / 100)
        dialog.window.attributes = attributes
    }

    private fun showCongratulationDialog(title: String, message: String, nextButtonText: String, @DrawableRes imageRes: Int, callback: () -> Unit) {
        val view = layoutInflater.inflate(R.layout.dialog_congrat_layout, null)
        val alert = AlertDialog.Builder(context)

        view.findViewById<FontChangeableTextView>(R.id.title).text = title
        view.findViewById<FontChangeableTextView>(R.id.message).text = message
        view.findViewById<FontChangeableTextView>(R.id.nextButtonText).text = nextButtonText
        view.findViewById<ImageView>(R.id.image).setImageResource(imageRes)

        alert.setView(view)
        alert.setCancelable(false)

        view.findViewById<LinearLayout>(R.id.nextLevelButton).setOnClickListener {
            callback()
            dialog.hide()
        }

        dialog = alert.create()

        if (!dialog.isShowing) {
            dialog.show()
        }

        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val point = Point()
        wm.defaultDisplay.getSize(point)
        val attributes = dialog.window.attributes
        attributes.width = (point.x * context.resources.getInteger(R.integer.dialog_percent) / 100)
        dialog.window.attributes = attributes
    }

    fun increaseRewardedCoins(amount: Int) {
        gamePresenter.increaseCoins(amount)
        if (dialog.isShowing) {
            dialog.hide()
        }
    }

    private fun initializeInjector() {
        getComponent(MenuActivityComponent::class.java).inject(this)
    }

    override fun levelLoaded(levelData: LevelData) {
        worldTable.setData(levelData)
    }

    override fun updateScore(score: Int) {
        totalWords.text = String.format(getString(R.string.score_value), score)
    }

    override fun updateCoins(coins: Int) {
        coinsCount.text = String.format(getString(R.string.coins_value), coins)
    }

    override fun levelIncreased() {
        //presenter.getLevel(menuNumber)
    }

    override fun wordSelected(menuItemNumber: Int, levelNumber: Int, wordNumber: Int, color: Int) {
        presenter.checkAllCharsSelected(menuItemNumber, levelNumber)
        updateViewWithSelectedWord(wordNumber, color)
    }

    private fun updateViewWithSelectedWord(wordNumber: Int, color: Int) {
        worldTable.getData()?.clearSelectable()
        worldTable.getData()?.setSelected(wordNumber, color)
        worldTable.setData(worldTable.getData()!!)
    }

    override fun levelCompleted(levelNumber: Int) {
        if (levelNumber == 8) {
            mainCallback.showInterstitialAdvert()
            levelIncreaserPresenter.finishMenuNumber(menuNumber)
        } else {
            if (levelNumber == 4) {
                mainCallback.showInterstitialAdvert()
            }
            levelIncreaserPresenter.increaseLevel(menuNumber)
            gamePresenter.increaseCoins(CoinValues.LEVEL_COMPLETED_REWARD)
            showCongratulationDialog("Awesome!", "You have done this level", "Next Level", R.drawable.ic_upvote, {
                presenter.getLevel(menuNumber)
            })
        }
    }

    override fun menuItemCompleted() {
        showCongratulationDialog("Congratulations!", "You have finished this category successfully",
                "Choose other category", R.drawable.ic_medal, {
            backLayout.performClick()
        })
    }

    override fun hintPosition(x: Int, y: Int) {
        val data = worldTable.getData()
        data?.setSelectable(x, y)
        worldTable.setData(data!!)
    }

    override fun hintPosition(x: FindWordUseCase.ReturnObject) {
        val data = worldTable.getData()
        x.positions.forEach {
            data?.setSelectable(it.first, it.second)
        }
        val word = ArrayList<WordTable.XYChar>()
        for (i in 0..x.word.length - 1) {
            val ch = x.word[i]
            val xCoord = x.positions[i].first
            val yCoord = x.positions[i].second
            word.add(WordTable.XYChar(xCoord, yCoord, ch))
        }
        worldTable.setSelectedWord(word)
        worldTable.setData(data!!)

        val layout = layoutInflater.inflate(R.layout.toast_hint_word, null)
        layout.message.text = x.word

        val toast = Toast(context)
        toast.setGravity(Gravity.BOTTOM, 0, pxFromDp(context, resources.getInteger(R.integer.toast_bottom).toFloat()).toInt())
        toast.duration = Toast.LENGTH_LONG
        toast.view = layout
        toast.show()
    }

    override fun coinsEnough(hintType: Int) {
        if (hintType == 1) {
            analyticsManager.logEvent("Main fragment", "letter hint ($menuName)")
            gamePresenter.increaseCoins(-CoinValues.LETTER_HINT_PAYMENT)
            presenter.findLetterHint(menuNumber)
        } else if (hintType == 2) {
            analyticsManager.logEvent("Main fragment", "word hint ($menuName)")
            gamePresenter.increaseCoins(-CoinValues.WORD_HINT_PAYMENT)
            presenter.findWordHint(menuNumber)
        }
    }

    override fun coinsNotEnough() {
        showGetCoinsDialog("Not enough coins. Get more")
    }

    override fun onError(message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun provideBoughtCoins(coinsQuantity: Int) {
        gamePresenter.increaseCoins(coinsQuantity)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainCallback) {
            mainCallback = context
        }
    }

    fun pxFromDp(context: Context, dp: Float): Float {
        return dp * context.resources.displayMetrics.density
    }

    interface MainCallback : DonateInterface {
        fun showRewardedAdvert()
        fun showInterstitialAdvert()
    }

    override val layoutResource: Int
        get() = R.layout.fragment_main_layout

}
