package com.vetalitet.findword.screens.main.presenters

import com.vetalitet.findword.domain.interactor.DefaultSingleObserver
import com.vetalitet.findword.domain.interactor.all.CheckCoinsAvailabilityUseCase
import com.vetalitet.findword.screens.main.presenters.view.CheckCoinsAvailabilityView
import com.vetalitet.findword.view.presenter.Presenter
import javax.inject.Inject

class CheckAvailabilityPresenter @Inject constructor(
        private val checkCoinsAvailabilityUseCase: CheckCoinsAvailabilityUseCase) : Presenter<CheckCoinsAvailabilityView>() {

    fun checkCoinsAvailability(hintType: Int, coins: Int) {
        val params = CheckCoinsAvailabilityUseCase.Params(coins)
        checkCoinsAvailabilityUseCase.execute(params, object : DefaultSingleObserver<Boolean>() {
            override fun onSuccess(t: Boolean) {
                super.onSuccess(t)
                if (t) {
                    view?.coinsEnough(hintType)
                } else {
                    view?.coinsNotEnough()
                }
            }

            override fun onError(e: Throwable) {
                super.onError(e)
                view?.onError(e.message)
            }
        })
    }

}