package com.vetalitet.findword

import android.app.Application
import com.google.android.gms.analytics.Tracker
import com.vetalitet.findword.di.component.ApplicationComponent
import com.vetalitet.findword.di.component.DaggerApplicationComponent
import com.vetalitet.findword.di.module.ApplicationModule
import com.vetalitet.findword.domain.domain.bootstrap.StorageInitializator
import javax.inject.Inject
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.analytics.GoogleAnalytics
import com.vetalitet.findword.managers.adv.Constants


class FindwordApplication: Application() {

    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
    }

    @Inject
    lateinit var storageInitializator: StorageInitializator

    var tracker: Tracker? = null

    override fun onCreate() {
        super.onCreate()
        initialize()
        initAdvert()
    }

    private fun initAdvert() {
        val isRealUser = resources.getBoolean(R.bool.isAdvRealUser)
        MobileAds.initialize(this, if (isRealUser) Constants.mobAdRealId else Constants.mobAdTestId)
    }

    fun getDefaultTracker() : Tracker {
        if (tracker == null) {
            val analytics = GoogleAnalytics.getInstance(this)
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            tracker = analytics.newTracker(R.xml.global_tracker)
        }
        return tracker!!
    }

    private fun initialize() {
        initInjector()
        initializeStorage()
    }

    private fun initializeStorage() {
        storageInitializator.configureStorage()
    }

    private fun initInjector() {
        applicationComponent.inject(this)
    }

}
