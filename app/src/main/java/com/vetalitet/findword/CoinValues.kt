package com.vetalitet.findword

class CoinValues {

    companion object {
        val LOW_PAID_REWARD = 250
        val HIGH_PAID_REWARD = 600
        //val FREE_COINS_REWARD = 25 -> moved to web (rewarded block)
        val LEVEL_COMPLETED_REWARD = 5

        val ENABLE_LEVEL_PAYMENT = 50
        val LETTER_HINT_PAYMENT = 15
        val WORD_HINT_PAYMENT = 30
    }

}