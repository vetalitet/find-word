package com.example.encrdecrlib;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class ConversionLib {

    private final static String ROOT_NAME = "";
    private final static String LIBRARY_NAME = "EncrDecrLib";
    private final static String MENUS_FOLDER_NAME = "menus";
    private final static String ENCRYPTED_MENUS_FOLDER_NAME = "menus_encrypted";

    public static void main(String[] args) {
        final File rootPath = new File(ROOT_NAME).getAbsoluteFile(); // D:\Applications\find-word
        final File libraryPath = new File(rootPath, LIBRARY_NAME);
        final File inputDir = new File(rootPath, MENUS_FOLDER_NAME);
        final File outputDir = new File(rootPath, ENCRYPTED_MENUS_FOLDER_NAME);
        encryptFiles(inputDir, outputDir);
        System.out.println("Done!");
    }

    private static void encryptFiles(File inputDir, File outputDir) {
        final File[] files = inputDir.listFiles();
        if (files != null && files.length > 0) {
            if (!outputDir.exists()) {
                outputDir.mkdir();
            }
            for (File file : files) {
                try {
                    final byte[] bytes = Files.readAllBytes(Paths.get(file.getCanonicalPath()));
                    writeToFile(encrypt(bytes), outputDir, file.getName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void writeToFile(byte[] value, File outputDir, String name) {
        final File outputFile = new File(outputDir, name);
        BufferedWriter writer = null;
        try {
            final OutputStream fileWriter = new FileOutputStream(outputFile);
            fileWriter.write(value);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private static final byte[] key = {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1};
    private static final byte[] iv = {1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0};

    private static final Cipher encryptCipher;
    private static final Cipher decryptCipher;

    static {
        try {
            encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            encryptCipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(iv));
            decryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            decryptCipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(iv));
        } catch (final NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static byte[] encrypt(byte[] value) {
        // encrypt
        final ByteArrayOutputStream encryptedPayloadOutputStream = new ByteArrayOutputStream();
        try (final InputStream inputStream = new ByteArrayInputStream(value);
             final OutputStream outputStream = new CipherOutputStream(encryptedPayloadOutputStream, encryptCipher)) {
            copy(inputStream, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encryptedPayloadOutputStream.toByteArray();
    }

    private static final int BUF_SIZE = 0x1000; // 4K

    private static long copy(InputStream from, OutputStream to)
            throws IOException {
        byte[] buf = new byte[BUF_SIZE];
        long total = 0;
        while (true) {
            int r = from.read(buf);
            if (r == -1) {
                break;
            }
            to.write(buf, 0, r);
            total += r;
        }
        return total;
    }

}
